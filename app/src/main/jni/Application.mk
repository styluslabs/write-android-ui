# previously used stlport_static, but that doesn't seem to support C++11
APP_STL := gnustl_static
APP_PLATFORM := android-16
# comment out the following line to disable HW FPU support
#  can also be set to 'all'
APP_ABI := armeabi-v7a
APP_CPPFLAGS := -fno-exceptions -fno-rtti -Wno-narrowing
