LOCAL_PATH := $(call my-dir)

scribble_path = /home/mwhite/scribble
skia_path = /home/mwhite/skia-old/trunk

include $(CLEAR_VARS)

LOCAL_MODULE := scribble

# get revision numbers from mercurial
# alternatively, this could run a script to get and increment a build number
ANDROID_REV_NUMBER := $(shell hg id -n)
SCRIBBLE_REV_NUMBER := $(shell hg id -n /home/mwhite/scribble)

# for verbose output, use ndk-build V=1
# need the android NDK flag for some of the skia headers
LOCAL_CFLAGS := -DSCRIBBLE_ANDROID -DSK_BUILD_FOR_ANDROID_NDK \
    -DANDROID_REV_NUMBER=$(ANDROID_REV_NUMBER) \
    -DSCRIBBLE_REV_NUMBER=$(SCRIBBLE_REV_NUMBER)

#-DDISABLE_IMPORTGL
# moved to Application.mk so that it appears after the -frtti that for some
#  reason appears on the compiler command line
#LOCAL_CPPFLAGS := -fno-exceptions -fno-rtti

scribble_srcs := \
    androidmain.cpp \
    scribbleconfig.cpp \
    scribblemode.cpp \
    scribbleinput.cpp \
    scribbleview.cpp \
    scribblearea.cpp \
    bookmarkview.cpp \
    document.cpp \
    undo.cpp \
    page.cpp \
    layer.cpp \
    stroke.cpp \
    strokebuilder.cpp \
    multistroke.cpp \
    selection.cpp \
    painter.cpp \
    basics.cpp \
    pugixml/pugixml.cpp

#LOCAL_SRC_FILES := $(scribble_srcs:%=$(scribble_path)/%)
LOCAL_SRC_FILES := $(addprefix $(scribble_path)/, $(scribble_srcs))

LOCAL_C_INCLUDES := \
    $(scribble_path) \
    $(scribble_path)/pugixml \
    $(skia_path)/include \
    $(skia_path)/include/core \
    $(skia_path)/include/images \
    $(skia_path)/include/config

# -lz needed for skia; choose Debug or Release (much faster) for skia
# NOTE: if you get linking errors (undefined ref), try changing the order of the
#  libraries (yes, it matters!).  Put inside the "group" if that doesn't work.
LOCAL_LDLIBS := \
    -llog -lz -landroid -ljnigraphics \
    -L$(skia_path)/out/Release/obj.target/gyp \
    -Wl,--start-group -lcore -lports -leffects -Wl,--end-group \
    -lutils -lopts -lft2 -limages -lpng -lexpat -lpdf -lzlib

# for Skia as of Nov 2015 - we should rebuild with SK_SFNTLY_SUBSETTER undef'ed to remove icu lib!
#    -llog -lz -landroid -ljnigraphics \
#    -L$(skia_path)/out/config/android-arm_v7_neon/Release \
#    -L$(skia_path)/out/config/android-arm_v7_neon/Release/obj/gyp \
#    -Wl,--start-group -lskia_core -lskia_ports -lskia_effects -lskia_skgpu -lskia_utils -lskia_pdf -Wl,--end-group \
#    -lskia_opts -lskia_opts_neon -lfreetype_static -lskia_images -lskia_sfnt -lpng_static -lpng_static_neon -lexpat -lsfntly -licuuc -letc1 -lSkKTX -lGLESv2 -lEGL

include $(BUILD_SHARED_LIBRARY)


