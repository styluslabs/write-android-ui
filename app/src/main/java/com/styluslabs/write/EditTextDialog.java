package com.styluslabs.write;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class EditTextDialog extends AlertDialog implements OnClickListener
{
  public interface OnTitleSetListener {
    void titleSet(String title);
  }

  private final Context mContext;
  private EditText mEditText;
  private final OnTitleSetListener mListener;
  private final String mTitle;
  private final String mPrompt;

  public EditTextDialog(Context context, OnTitleSetListener listener, String prompt, String title)
  {
  	super(context);
  	mContext = context;
  	mPrompt = prompt;
  	mTitle = title;
  	mListener = listener;
	}

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    setTitle(mPrompt);
    View view = getLayoutInflater().inflate(R.layout.title_setter, null);
    // OK, Cancel buttons
    setButton(DialogInterface.BUTTON_POSITIVE,
    		mContext.getText(R.string.confirm), this);
    setButton(DialogInterface.BUTTON_NEGATIVE,
    		mContext.getText(R.string.cancel), (OnClickListener) null);

    mEditText = (EditText) view.findViewById(R.id.edittitle);
    mEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      public boolean onEditorAction(TextView tv, int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_DONE
            && (event == null || event.getAction() == KeyEvent.ACTION_DOWN)) {
          doTitleSet();
          return true;
        }
        return false;
      }
    });
    mEditText.setText(mTitle);
    setView(view);
    // this cannot be called until after view is set
    super.onCreate(savedInstanceState);
  }

  // called for OK button click only
  public void onClick(DialogInterface dialog, int which) { doTitleSet(); }

  private void doTitleSet()
  {
    if(mListener != null) {
      mListener.titleSet(mEditText.getText().toString());
      dismiss();
    }
  }
}
