package com.styluslabs.write;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PaintMain extends Activity implements AdapterView.OnItemClickListener
{
  private static final int ACTIVITY_NEWPAINT = 0;
  private static final int ACTIVITY_EDITPAINT = 2;
  private static final int ACTIVITY_OPENFILE = 4;
  private static final int ACTIVITY_ADDFILE = 5;
  private static final int ACTIVITY_SAVEZIP = 6;

  //private static final float MIN_DRAG_DIST_SQ = 100*100;
  int mThumbWidth = 375;

  private NotesDbAdapter mDbHelper;
  //private Cursor notesCursor;
  private NoteListAdapter mNotesAdapter = null;
  private List<String> mTags;
  private String mSelectedTag = "";
  private Uri imageURI = null;
  private String mSortColumn;
  private int mNoteListPos = 0;

  SharedPreferences mPrefs;
  SharedPreferences.Editor mPrefEditor;
  private GridView mNotesList;
  private ListView mTagList;
  private View mNotesContainer;
  private TextView mNotesHeaderText;
  //int mListType;
  private String mExtStorageDir= "/sdcard";

  private int mMode;
  public static final String MODE = "MODE";
  public static final int MODE_NORMAL = 1; // open selecte note for editing
  public static final int MODE_SEL = 2;  // return id of selected note to parent activity
  public static final String SEL_NOTE_ID = "SEL_NOTE_ID";
  private static final String TAG_DELIM = ":::";

  private static final int VIEWSTATE_BOTH = 0;
  private static final int VIEWSTATE_TAGS = 1;
  private static final int VIEWSTATE_DOCS = 2;
  private int mViewState;


  public class NoteListAdapter extends CursorAdapter
  {
    private class NoteViewHolder
    {
      //long rowId;
      TextView text;
      ImageView icon;
    }

    private final LayoutInflater mInflater;

    public NoteListAdapter(Context context, Cursor c)
    {
      super(context, c);
      mInflater = LayoutInflater.from(context);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
      NoteViewHolder holder = (NoteViewHolder) view.getTag();
      String title = cursor.getString(cursor.getColumnIndex(NotesDbAdapter.KEY_TITLE));
      if(title == null || title.length() == 0)
        title = "";
      byte[] thumbraw = cursor.getBlob(cursor.getColumnIndex(NotesDbAdapter.KEY_THUMBNAIL));
      if(thumbraw != null && thumbraw.length > 0) {
        Bitmap thumb = BitmapFactory.decodeByteArray(thumbraw, 0, thumbraw.length);
        holder.icon.setImageBitmap(thumb);
        holder.icon.setAdjustViewBounds(true);
        holder.icon.setMaxHeight((5*mThumbWidth)/3);
      }
      holder.text.setText(title);
      //holder.rowId = cursor.getLong(cursor.getColumnIndex(NotesDbAdapter.KEY_ROWID));
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
      NoteViewHolder holder = new NoteViewHolder();
      // Creates a ViewHolder and store references to the two children views we want to bind data to.
      View view = mInflater.inflate(R.layout.grid_item, null);
      // can we use same ids for the two different layouts?
      holder.text = (TextView) view.findViewById(R.id.griditemtext);
      holder.icon = (ImageView) view.findViewById(R.id.griditemicon);
      view.setLayoutParams(new GridView.LayoutParams(mThumbWidth, LayoutParams.WRAP_CONTENT));
      view.setTag(holder);
      return view;
    }
  }

  public static int atoi(String a, int defaultval)
  {
    int i;
    try {
      i = Integer.parseInt(a);
    }
    catch(NumberFormatException nfe) {
      i = defaultval;
    }
    return i;
  }

  public static float atof(String s, float defaultval)
  {
    float i = defaultval;
    try {
      i = Float.parseFloat(s);
    }
    catch(NumberFormatException nfe) {
      try {
        i = NumberFormat.getInstance().parse(s).floatValue();
      }
      catch(ParseException e) { i = defaultval; }
    }
    return i;
  }

  @Override
  public void onCreate(Bundle savedstate)
  {
    super.onCreate(savedstate);
    setContentView(R.layout.notes_list);
    setTitle("Stylus Labs Write"); //R.string.app_name);
    mDbHelper = new NotesDbAdapter(this);
    mDbHelper.open();
    mExtStorageDir = Environment.getExternalStorageDirectory().getPath();

    Intent intent = getIntent();
    mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    mPrefEditor = mPrefs.edit();
    if(mPrefs.contains("NoteTags")) {
      mTags = new ArrayList<String>(Arrays.asList(mPrefs.getString("NoteTags", "").split(TAG_DELIM)));
      mTags.removeAll(Arrays.asList("", null));
    }
    else
      loadTagsFromDB();

    mThumbWidth = atoi(mPrefs.getString("ThumbnailSize", getString(R.string.pref_thumbnail_size_default)), mThumbWidth);
    mSelectedTag = mPrefs.getString("SelectedTag", "");
    mNoteListPos = mPrefs.getInt("NoteListPos", 0);
    mNotesList = (GridView) findViewById(R.id.notesgrid);
    registerForContextMenu(mNotesList);
    mMode = intent.getIntExtra(MODE, MODE_NORMAL);
    mNotesList.setOnItemClickListener(this);
    mNotesContainer = findViewById(R.id.notescontainer);
    mNotesHeaderText = (TextView) findViewById(R.id.notesheadertext);

    // setup tag list
    // NOTE: for the purposes of highlighting an item in a ListView, Android uses the term "checked", while
    //  "selected" is used to describe the item selected by a mouse or other non-touch input device
    // ListView.setChoiceMode is to be used with ListView.setItemChecked; for visual indication, the item
    //  must use a drawable that responds to the the activated property, e.g.,
    //  android:background="?android:attr/activatedBackgroundIndicator" (see simple_list_item_activated_1)
    mTagList = (ListView) findViewById(R.id.taglist);
    registerForContextMenu(mTagList);
    mTagList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        String tag = position > 0 ? mTags.get(position - 1) : "";
        if(mViewState != VIEWSTATE_BOTH) {
          mNotesContainer.setVisibility(View.VISIBLE);
          mTagList.setVisibility(View.GONE);
          mViewState = VIEWSTATE_DOCS;
        }
        if(tag != mSelectedTag) {
          mSelectedTag = tag;
          mNoteListPos = 0;
          fillNotes();
        }
      }
    });
    fillTags();

    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    // dipWidth is used to control how many icons get put on action bar
    if(metrics.widthPixels/metrics.density < 580) {
      mViewState = VIEWSTATE_TAGS;
      mNotesContainer.setVisibility(View.GONE);
    }
    else {
      mViewState = VIEWSTATE_BOTH;
      findViewById(R.id.notesheader).setVisibility(View.GONE);
      mTagList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
      mTagList.setItemChecked(mTags.indexOf(mSelectedTag) + 1, true);
    }

    // empty the temp folder
    if(mPrefs.contains("tempPath")) {
      File tempfolder = new File(mPrefs.getString("tempPath", ""));
      if(tempfolder.isDirectory()) {
        for(File child : tempfolder.listFiles())
          child.delete();
      }
    }
    // reference: http://developer.android.com/training/sharing/receive.html
    String action = intent.getAction();
    if(Intent.ACTION_SEND.equals(action)) {
      if(intent.getType() != null && intent.getType().startsWith("image/")) {
        imageURI = (Uri)intent.getParcelableExtra(Intent.EXTRA_STREAM);
        Toast.makeText(this, R.string.image_dest_msg, Toast.LENGTH_SHORT).show();
      }
    }
    else if(Intent.ACTION_VIEW.equals(action) || Intent.ACTION_EDIT.equals(action)) {
      if("text/html".equals(intent.getType()) && intent.getData() != null) {
        Intent i = new Intent(this, FingerPaint.class);
        i.putExtra("Filename", intent.getData().getPath());
        // prevent creation of DB entry
        i.putExtra(NotesDbAdapter.KEY_ROWID, -1L);
        // finish() removes PaintMain from the history stack; it would probably be better to move the
        //  intent-filter to the FingerPaint activity
        startActivity(i);
        finish();
      }
    }
  }

  private static final int STORAGE_PERMISSION = 0;
  private void requestStoragePermission() {
    if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
      requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    if(requestCode == STORAGE_PERMISSION) {
      if(grantResults.length < 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
        Toast.makeText(this, "Write cannot save documents without access to external storage", Toast.LENGTH_LONG).show();
        finish();
      }
      else
        onFirstRun();
    }
    else
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  // don't want these strings in a XML file - would be even easier to crack (not that I care)
  public static final String BASE_PACKAGE = "com.styluslabs.write";
  public static final String UNLOCK_PACKAGE = "com.styluslabs.write.plus";

  public static boolean isFullVersion(Context context)
  {
    // see http://www.marvinlabs.com/2011/01/sharing-code-full-lite-versions-application/
    // also http://code.google.com/p/libra-android/
    int match = context.getPackageManager().checkSignatures(BASE_PACKAGE, UNLOCK_PACKAGE);
    return (match == PackageManager.SIGNATURE_MATCH);
  }

  private void loadTagsFromDB()
  {
    mTags = new ArrayList<String>(Arrays.asList(mPrefs.getString("NoteTags", "").split(TAG_DELIM)));
    Cursor tagcursor = mDbHelper.fetchTags();
    while(tagcursor.moveToNext()) {
      String tag = tagcursor.getString(tagcursor.getColumnIndex(NotesDbAdapter.KEY_TAGS));
      if(!mTags.contains(tag))
        mTags.add(tag);
    }
    tagcursor.close();
    mTags.removeAll(Arrays.asList("", null));
    if(mTags.contains(mSelectedTag))
      mSelectedTag = "";
    saveTags();
    //fillNotes();
  }

  private void fillTags()
  {
    List<String> tags = new ArrayList<String>(mTags);
    tags.add(0, getString(R.string.unfiled));
    ArrayAdapter<String> tagAdapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.listitemtext, tags);
    mTagList.setAdapter(tagAdapter);
    if(mViewState == VIEWSTATE_BOTH)
      mTagList.setItemChecked(mTags.indexOf(mSelectedTag) + 1, true);
  }

  private void saveTags()
  {
    Collections.sort(mTags, String.CASE_INSENSITIVE_ORDER);
    mPrefEditor.putString("NoteTags", TextUtils.join(TAG_DELIM, mTags.toArray()));
    mPrefEditor.apply();
    mNoteListPos = 0;
    if(mTagList != null)
      fillTags();
  }

  private void fillNotes()
  {
    // do we always have to get a new cursor? what does a cursor cache?
    // Get all of the rows from the database and create the item list
    Cursor cursor = mDbHelper.fetchNotes(mSelectedTag, mSortColumn);
    if(mNotesAdapter != null)
      mNotesAdapter.changeCursor(cursor);
    else {
      mNotesAdapter = new NoteListAdapter(this, cursor);
      mNotesList.setAdapter(mNotesAdapter);
    }
    mNotesList.setColumnWidth(mThumbWidth);
    mNotesHeaderText.setText(mSelectedTag.isEmpty() ? getString(R.string.unfiled) : mSelectedTag);
  }

  private void showTutorialDoc()
  {
    try {
      String tempPath = Environment.getExternalStorageDirectory().getPath() + getString(R.string.tempdir);
      // don't include an extension since the filename will also be the Activity title
      File file = new File(tempPath, "Tutorial");
      // ensure that path exists
      if(!file.exists())
        file.getParentFile().mkdirs();
      FileOutputStream out = new FileOutputStream(file);
      // this returns InputStream object
      InputStream in = getResources().openRawResource(R.raw.writetut);
      // copy byte by byte
      byte[] buf = new byte[65536];
      int len;
      while((len = in.read(buf)) > 0)
        out.write(buf, 0, len);
      in.close();
      out.close();

      Intent intent = new Intent(this, FingerPaint.class);
      intent.putExtra("Filename", file.getPath());
      intent.putExtra("TitleString", getString(R.string.tutorial));
      // id < 0 (as opposed to null) indicates that FingerPaint should not create a DB entry
      intent.putExtra(NotesDbAdapter.KEY_ROWID, -1L);
      startActivityForResult(intent, ACTIVITY_EDITPAINT);
    }
    catch(IOException e) {
      // oh well, no tips
    }
  }

  // generate Zip file
  private void zipAllDocs(String outfilename)
  {
    File savefolder = new File(mPrefs.getString("SaveImgPath", ""));
    if(!savefolder.isDirectory())
      return;

    byte[] buf = new byte[65536];
    int len;
    try {
      FileOutputStream outfile = new FileOutputStream(outfilename);
      ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(outfile));
      try {
        for(File child : savefolder.listFiles()) {
          if(!child.isDirectory()) {
            ZipEntry entry = new ZipEntry(child.getName());
            zos.putNextEntry(entry);
            FileInputStream in = new FileInputStream(child);
            while((len = in.read(buf)) > 0)
              zos.write(buf, 0, len);
            in.close();
            zos.closeEntry();
          }
        }
      }
      finally {
        zos.close();
        outfile.close();
      }
    }
    // I don't give a FUUUUUCK!!!
    catch(Exception e) {}
  }

  private int getVersionCode() {
    try {
      return getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
    }
    catch(Exception e) {
      return 0;
    }
  }

  public static void showChangelog(Context context)
  {
    InputStream instr = context.getResources().openRawResource(R.raw.changelog);
    // trick to read entire stream as String
    java.util.Scanner sc = new java.util.Scanner(instr).useDelimiter("\\A");
    String changelog = sc.hasNext() ? sc.next() : "";
    // show in a webview
    WebView wv = new WebView(context);
    wv.setBackgroundColor(0); // transparent
    wv.loadDataWithBaseURL(null, changelog, "text/html", "UTF-8", null);

    new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK)  //.setIcon(R.drawable.ic_dialog_alert)
    .setTitle(R.string.changelog_title)
    .setView(wv)
    .setCancelable(false)
    .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
    }).show();
  }

  private void onFirstRun()
  {
    if(mPrefs.getBoolean("FirstRun", true)) {
      mPrefEditor.putBoolean("FirstRun", false);
      mPrefEditor.putInt("currVersionCode", getVersionCode());
      mPrefEditor.apply();
      String defpath = mExtStorageDir + getString(R.string.defaultdir);
      if(new File(defpath).isDirectory()) {
        Intent intent = new Intent(this, ImportActivity.class);
        intent.putExtra(ImportActivity.IMPORT_PATH, defpath);
        startActivity(intent);
      }
      else {
        createPaint();
        fillNotes();  // redraw
      }
    }
    else if(getVersionCode() > mPrefs.getInt("currVersionCode", 0)){
      mPrefEditor.putInt("currVersionCode", getVersionCode());
      mPrefEditor.apply();
      showChangelog(this);
    }
  }

  @Override
  protected void onPause()
  {
    super.onPause();
    imageURI = null;
    //mNoteListState = mNotesList.onSaveInstanceState();
    mNoteListPos = mNotesList.getFirstVisiblePosition();
    // we want these values to persist through restarts of activity, so we can't use onSaveInstanceState()
    mPrefEditor.putString("SelectedTag", mSelectedTag);
    mPrefEditor.putInt("NoteListPos", mNoteListPos);
    mPrefEditor.apply();
  }

  // based on Android activity lifecycle diagram, I removed fillData from onActivityResult and onCreate
  @Override
  protected void onResume()
  {
    super.onResume();

    // In case we're resuming from preferences ... technically, we should use
    //  onActivityResult and check for this case, I guess
    int thumbwidth = atoi(mPrefs.getString("ThumbnailSize", getString(R.string.pref_thumbnail_size_default)), mThumbWidth);
    if(thumbwidth != mThumbWidth) {
      mThumbWidth = thumbwidth;
      mNotesAdapter = null;  // force creation of new adapter to update layout params of grid items
    }
    int sortorder = atoi(mPrefs.getString("DocSortOrder", getString(R.string.pref_docsort_default)), 0);
    if(sortorder == 0)
      mSortColumn = NotesDbAdapter.KEY_CREATETIME + " DESC";
    else if(sortorder == 1)
      mSortColumn = NotesDbAdapter.KEY_MODIFYTIME + " DESC";
    else //if(sortorder == 2)
      mSortColumn = NotesDbAdapter.KEY_TITLE + " ASC";
    fillNotes();
    if(mNoteListPos > 0)
      mNotesList.setSelection(mNoteListPos);  //mNotesList.onRestoreInstanceState(mNoteListState);

    // for Android 23+
    if(android.os.Build.VERSION.SDK_INT >= 23)
      requestStoragePermission();
    else
      onFirstRun();
  }

  // Is this right?
  @Override
  protected void onDestroy()
  {
    // onDestroy is called for orientation change; when Android is actually
    //  done with our application, it will free all memory
    //FingerPaint.jniExit();
    /* if(notesCursor != null) {
      notesCursor.close();
      notesCursor = null;
    } */
    mDbHelper.close();
    mDbHelper = null;
    super.onDestroy();
  }

  //private static final int TOGGLELIST_ID = Menu.FIRST;
  private static final int NEWFOLDER_ID = Menu.FIRST;
  private static final int NEWNOTE_ID = Menu.FIRST + 1;
  private static final int OPENFILE_ID = Menu.FIRST + 2;
  private static final int ADDFILE_ID = Menu.FIRST + 3;
  private static final int SAVEZIP_ID = Menu.FIRST + 4;
  private static final int HELP_ID = Menu.FIRST + 6;
  private static final int PREFS_ID = Menu.FIRST + 7;
  // context menu
  private static final int DELETENOTE_ID = Menu.FIRST + 100;
  private static final int RENAMENOTE_ID = Menu.FIRST + 101;
  private static final int MOVENOTE_ID = Menu.FIRST + 102;
  private static final int DELETEFOLDER_ID = Menu.FIRST + 200;
  private static final int RENAMEFOLDER_ID = Menu.FIRST + 201;

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    super.onCreateOptionsMenu(menu);
    if(mMode != MODE_SEL) {
      menu.add(0, NEWFOLDER_ID, 0, R.string.menu_newfolder)
          .setIcon(R.drawable.ic_menu_add_folder)
          .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
      menu.add(0, NEWNOTE_ID, 0, R.string.menu_insertnote)
          .setIcon(R.drawable.ic_menu_add_doc)
          .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
    }
    // grid/list toggle is setup each time menu is displayed
    //menu.add(0, TOGGLELIST_ID, 0, R.string.menu_viewaslist).setIcon(R.drawable.ic_menu_add);
    if(mMode != MODE_SEL) {
      menu.add(0, OPENFILE_ID, 0, R.string.menu_openfile).setIcon(R.drawable.ic_menu_folder);
      menu.add(0, ADDFILE_ID, 0, R.string.menu_addfile).setIcon(R.drawable.ic_menu_folder);
      menu.add(0, SAVEZIP_ID, 0, R.string.menu_exportdocs).setIcon(R.drawable.ic_menu_folder);
      menu.add(0, HELP_ID, 0, R.string.menu_help).setIcon(R.drawable.ic_menu_help);
      menu.add(0, PREFS_ID, 0, R.string.menu_prefs).setIcon(R.drawable.ic_menu_preferences);
    }
    return true;
  }

  @Override
  public boolean onMenuItemSelected(int featureId, MenuItem item)
  {
    int id = item.getItemId();
    Intent intent;
    switch(id) {
    case NEWFOLDER_ID:
      String newfolder = getString(R.string.menu_newfolder);
      // find an unused name
      for(int count = 1; mTags.contains(newfolder); count++) {
        newfolder = getString(R.string.menu_newfolder) + " (" + Integer.toString(count) + ")";
      }
      mSelectedTag = newfolder;
      mTags.add(newfolder);
      saveTags();
      fillNotes();
      // let's prompt immediately for name
      new EditTextDialog(this, new RenameFolderListener(newfolder),
          getString(R.string.edit_tag), newfolder).show();
      return true;
    case NEWNOTE_ID:
      createPaint();
      return true;
    case OPENFILE_ID:
    case ADDFILE_ID:
      intent = new Intent(this, FileBrowser.class);
      intent.putExtra(FileBrowser.START_PATH, mPrefs.getString("OpenFilePath", mExtStorageDir));
      startActivityForResult(intent,
          id == OPENFILE_ID ? ACTIVITY_OPENFILE : ACTIVITY_ADDFILE);
      return true;
    case SAVEZIP_ID:
      intent = new Intent(this, FileBrowser.class);
      intent.putExtra(FileBrowser.START_PATH, mPrefs.getString("ZipFilePath", mExtStorageDir));
      intent.putExtra(FileBrowser.START_NAME, "WriteBackup.zip");
      intent.putExtra(FileBrowser.MODE, FileBrowser.MODE_SAVE);
      startActivityForResult(intent, ACTIVITY_SAVEZIP);
      return true;
    case HELP_ID:
      showTutorialDoc();
      return true;
    case PREFS_ID:
      startActivity(new Intent(this, PaintPreferences.class));
      return true;
    }

    return super.onMenuItemSelected(featureId, item);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
  {
    super.onCreateContextMenu(menu, v, menuInfo);
    if(v == mNotesList) {
      menu.add(0, DELETENOTE_ID, 0, R.string.menu_deletedoc);
      menu.add(0, RENAMENOTE_ID, 0, R.string.menu_rename);
      menu.add(0, MOVENOTE_ID, 0, R.string.menu_movenote);
    }
    else if(v == mTagList) {
      if(((AdapterView.AdapterContextMenuInfo)menuInfo).position > 0) {
        menu.add(0, DELETEFOLDER_ID, 0, R.string.menu_deletefolder);
        menu.add(0, RENAMEFOLDER_ID, 0, R.string.menu_rename);
      }
    }
  }

  private class RenameNoteListener implements EditTextDialog.OnTitleSetListener
  {
    private final long mId;

    public RenameNoteListener(long id) {
      mId = id;
    }
    public void titleSet(String title) {
      mDbHelper.updateNote(mId, NotesDbAdapter.KEY_TITLE, title);
      fillNotes();
    }
  }

  private class RenameFolderListener implements EditTextDialog.OnTitleSetListener
  {
    private final String mOldtag;

    public RenameFolderListener(String oldtag) {
      mOldtag = oldtag;
    }
    public void titleSet(String newtag) {
      if(newtag != null && newtag.length() > 0) {
        mDbHelper.renameTag(mOldtag, newtag);
        mTags.remove(mOldtag);
        mTags.remove(newtag);
        mTags.add(newtag);
        mSelectedTag = newtag;
        saveTags();
        // this is necessary in case folder already existed
        fillNotes();
      }
    }
  }

  private void moveNotePrompt(long _rowid)
  {
    final long rowid = _rowid;
    new AlertDialog.Builder(this) //.setIcon(R.drawable.ic_dialog_alert)
        .setTitle(R.string.msg_movenote)
        .setAdapter(mTagList.getAdapter(), new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            String tag = which > 0 ? mTags.get(which - 1) : "";
            mDbHelper.updateNote(rowid, NotesDbAdapter.KEY_TAGS, tag);
            fillNotes();
          }
    }).show();
  }

  @Override
  public boolean onContextItemSelected(MenuItem item)
  {
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
    Cursor note;
    switch(item.getItemId()) {
    case DELETENOTE_ID:
      note = mDbHelper.fetchNote(info.id);
      String body = note.getString(note.getColumnIndex(NotesDbAdapter.KEY_BODY));
      note.close();
      if(body != null && body.length() > 1 && body.charAt(0) == '\254')
        warnDeleteFile(body.substring(1), info.id);
      else
        warnDeleteFile(null, info.id);
      // delete entry from notes list
      //mDbHelper.deleteNote(info.id);
      //fillNotes();
      return true;
    case RENAMENOTE_ID:
      note = mDbHelper.fetchNote(info.id);
      String mTitleString = note.getString(note.getColumnIndex(NotesDbAdapter.KEY_TITLE));
      note.close();
      new EditTextDialog(this, new RenameNoteListener(info.id), getString(R.string.edit_title),
          mTitleString).show();
      return true;
    case MOVENOTE_ID:
      moveNotePrompt(info.id);
      return true;
    case DELETEFOLDER_ID:
      if(info.position > 0) {
        String tag = mTags.get(info.position - 1);
        mDbHelper.deleteTag(tag);
        mTags.remove(tag);
        saveTags();
        if(mSelectedTag == tag)
          mSelectedTag = "";
        fillNotes();
      }
      return true;
    case RENAMEFOLDER_ID:
      if(info.position > 0) {
        String oldtag = mTags.get(info.position - 1);
        new EditTextDialog(this, new RenameFolderListener(oldtag),
            getString(R.string.edit_tag), oldtag).show();
      }
      return true;
    default:
      return super.onContextItemSelected(item);
    }
  }

  private void createPaint()
  {
    Intent i = new Intent(this, FingerPaint.class);
    i.putExtra(NotesDbAdapter.KEY_TAGS, mSelectedTag);
    if(imageURI != null)
      i.putExtra("imageURI", imageURI);
    startActivityForResult(i, ACTIVITY_NEWPAINT);
  }

  public void onItemClick(AdapterView<?> parent, View v, int position, long id)
  {
    if(mMode == MODE_SEL) {
      // return selected note if in slave mode
      getIntent().putExtra(SEL_NOTE_ID, (int)id);
      setResult(RESULT_OK, getIntent());
      finish();
    }
    else {
      Cursor note = mDbHelper.fetchNote(id);
      Intent i;
      i = new Intent(this, FingerPaint.class);
      i.putExtra(NotesDbAdapter.KEY_ROWID, id);
      if(imageURI != null)
        i.putExtra("imageURI", imageURI);
      i.putExtra(NotesDbAdapter.KEY_TAGS, note.getString(note.getColumnIndex(NotesDbAdapter.KEY_TAGS)));
      startActivityForResult(i, ACTIVITY_EDITPAINT);
      note.close();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent)
  {
    if(requestCode == ACTIVITY_NEWPAINT && resultCode == RESULT_OK) {
      mNoteListPos = 0;  // reset view to top of list
    //FingerPaint.cleanUp();
    }
    else if(requestCode == ACTIVITY_EDITPAINT) {
      if(resultCode == FingerPaint.RESULT_LOADFAILED) {
        new AlertDialog.Builder(this).setIcon(R.drawable.ic_dialog_alert)
        .setTitle(R.string.error)
        .setCancelable(false)
        .setMessage("Error loading library - please uninstall and reinstall Write.  Contact support@styluslabs.com if problem persists.")
        .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) { }
        }).show();
      }
      //FingerPaint.cleanUp();
    }
    else if((requestCode == ACTIVITY_ADDFILE || requestCode == ACTIVITY_OPENFILE) && resultCode == RESULT_OK) {
      String filePath = intent.getStringExtra(FileBrowser.RESULT_PATH);
      // save the path
      mPrefEditor.putString("OpenFilePath", filePath);
      mPrefEditor.apply();
      Intent i = new Intent(this, FingerPaint.class);
      i.putExtra("Filename", filePath);
      // id < 0 (as opposed to null) indicates that FingerPaint should not create a DB entry
      if(requestCode == ACTIVITY_OPENFILE)
        i.putExtra(NotesDbAdapter.KEY_ROWID, -1L);
      else if(requestCode == ACTIVITY_ADDFILE) {
        i.putExtra(NotesDbAdapter.KEY_TAGS, mSelectedTag);
        // remove if already in DB
        Cursor note = mDbHelper.fetchNoteWithBody("\254" + filePath);
        if(note != null && note.getCount() > 0)
          mDbHelper.deleteNote(note.getLong(note.getColumnIndex(NotesDbAdapter.KEY_ROWID)));
      }
      startActivityForResult(i, ACTIVITY_EDITPAINT);
    }
    else if(requestCode == ACTIVITY_SAVEZIP && resultCode == RESULT_OK) {
      String filePath = intent.getStringExtra(FileBrowser.RESULT_PATH);
      // save the path
      mPrefEditor.putString("ZipFilePath", filePath);
      mPrefEditor.apply();
      zipAllDocs(filePath);
    }
    super.onActivityResult(requestCode, resultCode, intent);
  }

  @Override
  public boolean dispatchKeyEvent(KeyEvent event)
  {
    switch(event.getKeyCode()) {
    case KeyEvent.KEYCODE_BACK:
      if(mViewState == VIEWSTATE_DOCS) {
        mTagList.setVisibility(View.VISIBLE);
        mNotesContainer.setVisibility(View.GONE);
        mViewState = VIEWSTATE_TAGS;
        return true;
      }
    default:
      break;
    }
    return super.dispatchKeyEvent(event);
  }

  private void warnDeleteFile(String _filename, long _rowid)
  {
    final String filename = _filename;
    final long rowid = _rowid;

    new AlertDialog.Builder(this).setIcon(R.drawable.ic_dialog_alert)
    .setTitle(R.string.confirm_delete_title)
    .setMessage(R.string.confirm_delete_msg)
    .setPositiveButton(R.string.yes,
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          mDbHelper.deleteNote(rowid);
          if(filename != null)
            FingerPaint.jniDeleteFile(filename);
          fillNotes();
        }
      })
    .setNeutralButton(R.string.no,
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          mDbHelper.deleteNote(rowid);
          fillNotes();
        }
      })
    .setNegativeButton(R.string.cancel,
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {}
      }).show();
  }
}

// Drag and drop for moving notes - not going to use for now
/* class NoteTouchListener implements View.OnTouchListener
{
  float downX, downY;
  boolean dragging = false;
  public boolean onTouch(View v, MotionEvent event)
  {
    if(event.getAction() == MotionEvent.ACTION_DOWN) {
      downX = event.getX();
      downY = event.getY();
      dragging = false;
    }
    else if(!dragging && event.getAction() == MotionEvent.ACTION_MOVE) {
      float dx = event.getX() - downX;
      float dy = event.getY() - downY;
      if(dx*dy + dy*dy > MIN_DRAG_DIST_SQ) {
        dragging = true;
        ClipData clipdata = ClipData.newPlainText("", "");
        View.DragShadowBuilder shadow = new View.DragShadowBuilder(v);
        v.startDrag(clipdata, shadow, v, 0);
      }
    }
    return false;
  }
}

class TagListDragListener implements View.OnDragListener
{
  public boolean onDrag(View v, DragEvent event)
  {
    switch(event.getAction()) {
    case DragEvent.ACTION_DRAG_STARTED:
      break;
    case DragEvent.ACTION_DRAG_ENTERED:
      v.setBackgroundDrawable(new ColorDrawable(0xBF0000FF));
      break;
    case DragEvent.ACTION_DRAG_EXITED:
      v.setBackgroundDrawable(null);
      break;
    case DragEvent.ACTION_DROP:
      View droppedview = (View) event.getLocalState();
      NoteViewHolder holder = (NoteViewHolder) droppedview.getTag();
      TextView textview = (TextView)v.findViewById(R.id.listitemtext);
      mDbHelper.updateNote(holder.rowId, NotesDbAdapter.KEY_TAGS, textview.getText().toString());
      fillNotes();
      v.setBackgroundDrawable(null);
      break;
    case DragEvent.ACTION_DRAG_ENDED:
      v.setBackgroundDrawable(null);
    default:
      break;
    }
    return true;
  }
} */
