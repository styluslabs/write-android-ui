package com.styluslabs.write;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class ToolButton extends ImageView implements View.OnClickListener, PopupWindow.OnDismissListener
{

  public interface ViewSelectedListener
  {
    void viewSelected(ToolButton toolbtn, View view);
    void viewPressed(ToolButton toolbtn, boolean down, boolean forceupdate);
  }

  private ViewSelectedListener mListener;
  private PopupWindow mPopupMenu;
  private ViewGroup mMenuLayout;
  private boolean pressOpen = true;
  private boolean isPopupOpened = false;
  private boolean allowSelection = false;
  private View activeMenuView = null;
  private View selectedView = null;
  private final LayoutInflater inflater;
  private final Context mContext;

  private static final int PRESSED_COLOR = 0xBF0000FF; //0xff00ddff; //android.R.color.holo_blue_bright; //0x5F7F7FFF;  // button or row pressed
  private static final int SELECTED_COLOR = 0x5F0000FF; //0xff0099cc; //android.R.color.holo_blue_dark; //0x5F00FFFF;  // row selected
  private static final int BUTTON_ACTIVE_COLOR = 0x5F0000FF; //0xff0099cc; //android.R.color.holo_blue_dark; //0x5F0000FF;
  private static final int BUTTON_LOCKED_COLOR = 0xBF0000FF; //0xff33b5e5; // android.R.color.holo_blue_light; //0x5F3F3FFF;


  public ToolButton(Context context)
  {
    this(context, null, 0);
  }

  public ToolButton(Context context, AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public ToolButton(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    mContext = context;
    setClickable(true);
    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  public void setListener(ViewSelectedListener cb, boolean pressopen)
  {
    mListener = cb;
    pressOpen = pressopen;
  }

  public void addMenuItem(int iconres, int titleid, Integer tag)
  {
    ensureMenuLayout();
    // no easy way to clone a View ... just have to reinflate
    ViewGroup row = (ViewGroup) inflater.inflate(R.layout.menu_row, null);
    row.setId(View.NO_ID);
    row.setTag(tag);
    row.setOnClickListener(this);
    ((ImageView)row.getChildAt(0)).setImageResource(iconres);
    ((TextView)row.getChildAt(1)).setText(titleid);
    mMenuLayout.addView(row);
  }

  /* public void setMenu(int menures)
  {
    mMenuLayout = (ViewGroup) inflater.inflate(menures, null);
    // TODO: get rid of PopupWindow fly-in animation
    mPopupMenu = new PopupWindow(mMenuLayout,
        WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
  } */

  public PenPreviewView addPenPreview(Integer tag)
  {
    ensureMenuLayout();
    ViewGroup row = (ViewGroup) inflater.inflate(R.layout.pen_preview_row, null);
    row.setId(View.NO_ID);
    row.setTag(tag);
    row.setOnClickListener(this);
    ((ImageView)row.getChildAt(0)).setImageResource(R.drawable.ic_menu_draw);
    mMenuLayout.addView(row);
    return (PenPreviewView)row.getChildAt(1);
  }

  private void ensureMenuLayout()
  {
    if(mMenuLayout == null) {
      LinearLayout ll = new LinearLayout(mContext);
      // android:layout_marginBottom="4dp" ... LayoutParams.setMargins()
      ll.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
      ll.setOrientation(LinearLayout.VERTICAL);
      ll.setBackgroundColor(Color.BLACK);
      mMenuLayout = ll;
      // TODO: get rid of PopupWindow fly-in animation
      mPopupMenu = new PopupWindow(mMenuLayout,
          WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
      // these are required for the PopupWindow to dismiss when a touch occurs elsewhere
      // setOutsideTouchable(true) causes touch event which causes dismiss to also be delivered to
      //  outside widget, which is not desirable.  I'm not sure if setFocusable is necessary
      mPopupMenu.setBackgroundDrawable(new BitmapDrawable());
      //mPopupMenu.setOutsideTouchable(true);
      mPopupMenu.setFocusable(true);
      mPopupMenu.setOnDismissListener(this);
    }
  }

  void setActive(int active, int mode)
  {
    if(mMenuLayout != null) {
      selectedView = null;
      for(int ii = 0; ii < mMenuLayout.getChildCount(); ii++) {
        View v = mMenuLayout.getChildAt(ii);
        if(v.getTag() != null && ((Integer)v.getTag()).intValue() == mode) {
          v.setBackgroundColor(SELECTED_COLOR);
          selectedView = v;
          if(mode < 100)
            setImageDrawable(((ImageView)((ViewGroup)v).getChildAt(0)).getDrawable());
        }
        else
          v.setBackgroundColor(0x00000000);
      }
    }
    setActive(active);
  }

  void setActive(int active)
  {
    if(active == 0)
      setBackgroundColor(0x00000000);
    else if(active == 1)
      setBackgroundColor(BUTTON_ACTIVE_COLOR);
    else
      setBackgroundColor(BUTTON_LOCKED_COLOR);
  }

  // called to indicate that we should not show the drop down on release (for simul. pen + touch)
  void disallowSelection()
  {
    allowSelection = false;
  }

  View getActiveView()
  {
    return (activeMenuView != null) ? activeMenuView : this;
  }

  private boolean isPointInsideView(float x, float y, View view)
  {
    int location[] = new int[2];
    view.getLocationOnScreen(location);
    int viewX = location[0];
    int viewY = location[1];
    //point is inside view bounds
    return (( x > viewX && x < (viewX + view.getWidth())) && ( y > viewY && y < (viewY + view.getHeight())));
  }

  private void selectAndDismiss()
  {
    if(isPopupOpened) {
      isPopupOpened = false;
      mPopupMenu.dismiss();
    }
    // must be before callback, which may change backgrounds
    setBackgroundColor(0x00000000);
    // for the next time the menu is opened
    if(activeMenuView != null) {
      activeMenuView.setBackgroundColor(0x00000000);
      // if menu item represents a mode (tag < 100), change action bar icon to match
      // ... not necessary - will get handled in setActive()
      //if(activeMenuView.getTag() != null && ((Integer)activeMenuView.getTag()).intValue() < 100)
      //  setImageDrawable(((ImageView)((ViewGroup)activeMenuView).getChildAt(0)).getDrawable());
    }
    if(mListener != null && allowSelection) {
      mListener.viewSelected(this, activeMenuView != null ? activeMenuView : this);
    }
    activeMenuView = null;
  }

  // listener for PopupWindow dismissal; ideally, this would be treated as a cancel, so we would not call
  //  viewSelected listener, but it's easier to treat it as a click on the tool button
  public void onDismiss()
  {
    if(isPopupOpened) {
      isPopupOpened = false;
      selectAndDismiss();
    }
  }

  // onClick listener for menu item - called if item is clicked with second finger after opening menu or if
  //  pressOpen is false and user taps on menu item
  public void onClick(View v)
  {
    if(activeMenuView == null)
      activeMenuView = v;
    selectAndDismiss();
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    final int action = event.getAction();

    switch (action) {
    case MotionEvent.ACTION_DOWN:
      if(mPopupMenu != null && !isPopupOpened && pressOpen) {
        // show as a dropdown from this view
        mPopupMenu.showAsDropDown(this);
        isPopupOpened = true;
      }
      allowSelection = true;
      setBackgroundColor(PRESSED_COLOR);
      mListener.viewPressed(this, true, false);
      //setPressed(true);
      return true;
    case MotionEvent.ACTION_MOVE:
      if(!isPopupOpened)
        return true;
      // highlight the popup menu item that the touch point is currently over
      if(activeMenuView != null) {
        activeMenuView.setBackgroundColor(0x00000000);
        activeMenuView = null;
      }
      // make sure we haven't cleared selected view's background
      if(selectedView != null)
        selectedView.setBackgroundColor(SELECTED_COLOR);
      for(int ii = 0; ii < mMenuLayout.getChildCount(); ii++) {
        View view = mMenuLayout.getChildAt(ii);
        if(isPointInsideView(event.getRawX(), event.getRawY(), view)) {
          activeMenuView = view;
          activeMenuView.setBackgroundColor(PRESSED_COLOR);
          break;
        }
      }
      return true;
    case MotionEvent.ACTION_UP:
      if(mPopupMenu != null && !isPopupOpened && !pressOpen && allowSelection) {
        // show as a dropdown from this view
        mPopupMenu.showAsDropDown(this);
        isPopupOpened = true;
      }
      else
        selectAndDismiss();
      mListener.viewPressed(this, false, !allowSelection);
      return true;
    }
    return false;
  }
}
