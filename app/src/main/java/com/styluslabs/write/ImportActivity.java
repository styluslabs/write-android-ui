package com.styluslabs.write;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ImportActivity extends Activity
{
  public static final String IMPORT_PATH = "importPath";
  private static final int ACTIVITY_IMPORTDOC = 1;
  ArrayList<File> mFiles;
  NotesDbAdapter mDbHelper;
  ProgressDialog progressDialog;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    mDbHelper = new NotesDbAdapter(this);
    mDbHelper.open();
    File folder = new File(getIntent().getStringExtra(IMPORT_PATH));
    // try default
    if(!folder.isDirectory())
      folder = new File(Environment.getExternalStorageDirectory().getPath() + getString(R.string.defaultdir));

    File[] files = folder.listFiles(new FilenameFilter() {
      public boolean accept(File parentdir, String name) {
        return name.toLowerCase().endsWith(".html");
      }
    });
    if(files == null) {
      Toast.makeText(getApplicationContext(), R.string.noimport_msg, Toast.LENGTH_SHORT).show();
      finish();
      return;
    }
    mFiles = new ArrayList<File>(Arrays.asList(files));
    // normally, we'd have to use a Thread or AsyncTask to show a ProgressDialog, but since we're
    //  going back to the main message loop, this does work here
    progressDialog = ProgressDialog.show(this, "", getResources().getString(R.string.importing_msg));
    progressDialog.setIndeterminate(true);
    progressDialog.setCancelable(false);
    progressDialog.setCanceledOnTouchOutside(false);
    onActivityResult(ACTIVITY_IMPORTDOC, RESULT_OK, null);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent resintent)
  {
    if(requestCode == ACTIVITY_IMPORTDOC) {
      while(mFiles.size() > 0) {
        // seriously Java?  C++ containers all provide pop_back()
        File file = mFiles.remove(mFiles.size() - 1);
        // don't import file already in DB
        Cursor note = mDbHelper.fetchNoteWithBody("\254" + file.getPath());
        if(note == null || note.getCount() == 0) {
          Intent intent = new Intent(this, FingerPaint.class);
          intent.putExtra("Filename", file.getPath());
          intent.putExtra("importOnly", true);
          startActivityForResult(intent, ACTIVITY_IMPORTDOC);
          return;
        }
      }
      // force update of tags
      SharedPreferences.Editor prefedit = PreferenceManager.getDefaultSharedPreferences(this).edit();
      prefedit.remove("NoteTags");
      prefedit.apply();
      // no more files left
      progressDialog.dismiss();
      mDbHelper.close();
      mDbHelper = null;
      // return to PaintMain
      Intent intent = new Intent(this, PaintMain.class);
      // this is supposed to remove other activities from the history stack
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
      finish();
    }
    else
      super.onActivityResult(requestCode, resultCode, resintent);
  }

}
