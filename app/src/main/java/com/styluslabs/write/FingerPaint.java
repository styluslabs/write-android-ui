package com.styluslabs.write;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

//import android.util.Log;

// Debugging note: must restart adb as su in order to access devices over USB:
//  http://hello-world-2-0.blogspot.com/2010/01/using-android-debug-bridge-adb-in-linux.html

public class FingerPaint extends Activity implements ToolPickerDialog.OnToolChangedListener,
    EditTextDialog.OnTitleSetListener, PageSetupDialog.OnPageSetupListener, ToolButton.ViewSelectedListener
{
  // private NotesDbAdapter mDbHelper;
  private MyView myview = null;
  private Menu mMenu = null;
  private Handler mHandler = null;
  private Runnable mTimerTask = null;

  private Long mRowId;
  //private Long mSortOrder;
  private String mNoteTag;
  private String mFilename;
  private boolean mCancel = false;
  private String mTitleString;
  private NotesDbAdapter mDbHelper;
  private int dipWidth;
  private int dipHeight;
  private SharedPreferences mPrefs;
  private SharedPreferences.Editor prefEditor;

  private int volUpAction = 0;
  private int volDownAction = 0;
  private int volUpState = KeyEvent.ACTION_UP;
  private int volDownState = KeyEvent.ACTION_UP;

  private boolean apiLevel14 = false;
  private PenPreviewView lastPen;

  private interface ImageInputStreamFactory {
    InputStream getStream() throws FileNotFoundException;
  }

  // JNI
  public static native void jniInit();
  public static native void jniExit();
  public static native boolean jniDeleteFile(String filename);

  private static native boolean jniLoadGlobalConfig(String prefsxml);
  private static native boolean jniLoadDocConfig(String prefsxml);
  private static native String jniGetConfigValue(String cfgname);
  private static native boolean jniSetDocConfigValue(String cfgname, String cfgval);
  private static native boolean jniSetGlobalConfigValue(String cfgname, String cfgval);
  private static native int[] jniGetUIState();
  private static native void jniItemClick(int itemid);
  private static native void jniSetPen(int c, float w, float pr, boolean highlight);
  private static native int[] jniGetPageProps();
  private static native boolean jniSetPageProps(String s);
  private static native void jniInsertImage(Bitmap bitmap);
  private static native boolean jniCmpFilename(String filename);
  private static native int jniOpenFile(String filename);
  private static native boolean jniSetFilename(String filename);
  private static native boolean jniSaveFile();
  private static native boolean jniSavePNG(String filename);
  private static native boolean jniSaveHTML(String filename);
  private static native boolean jniSavePDF(String filename);
  private static native boolean jniTimerEvent();
  // used in MyView
  private static native void jniPaint(Surface s);
  private static native void jniPaintThumb(Bitmap b);
  private static native void jniInputStart(int source, int modemod, int pressedmode);
  private static native void jniTwoInput(float x1, float y1, float x2, float y2, int eventtype);
  private static native void jniSingleInput(float x, float y, float pressure, int eventtype);
  private static native boolean jniCancelLastPan();
  // for links
  private static native String jniGetHyperRef();
  private static native void jniCreateHyperRef(String href);
  private static native String jniGetClickedLink();

  private static boolean staticLoad = true;
  private static boolean loadLibFailed = false;
  public static final int RESULT_LOADFAILED = RESULT_FIRST_USER + 1;
  // load native library
  static {
    try {
      System.loadLibrary("scribble");
      // activity is destroyed and recreated on every orientation
      //  change, but we only want to call jniInit() once
      jniInit();
      loadLibFailed = false;
    }
    catch(UnsatisfiedLinkError e) {
      loadLibFailed = true;
    }
  }

  // Use cases:
  // - open file, no DB ... filename passed, row id = -1
  // - open file, add to DB ... filename passed, row id = null
  // - new file, discard changes, no DB ... saveState returns if canceling
  // - new file, add to DB ... in saveState; filename is set when new doc is created
  // - open from DB
  // We must write to DB in onPause because we may never return to PaintMain

  // Remaining issue: open from file, no DB, pause ... we lose filename.
  // ... everything will be fine since filename is in native module, with the exception
  // that title will change to "Untitled" on resume.  Alternatively, we could get it
  // from intent.

  // Activity lifecycle methods
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    if(loadLibFailed) {
      setResult(RESULT_LOADFAILED);
      finish();
      return;
    }
    // only possible result at the moment
    setResult(RESULT_OK);

    mDbHelper = new NotesDbAdapter(this);
    mDbHelper.open();
    // getIntent returns the Intent object which spawned this activity
    // If we are to edit an existing note, the Intent obj will contain
    // the necessary info as "extras"
    mRowId = null;
    mFilename = null;
    mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    prefEditor = mPrefs.edit();
    Bundle extras = getIntent().getExtras();
    if(savedInstanceState == null && extras != null) {
      if(extras.containsKey(NotesDbAdapter.KEY_ROWID))
        mRowId = extras.getLong(NotesDbAdapter.KEY_ROWID);
      //if(extras.containsKey(NotesDbAdapter.KEY_SORTORDER))
      //  mSortOrder = extras.getLong(NotesDbAdapter.KEY_SORTORDER);
      if(extras.containsKey(NotesDbAdapter.KEY_TAGS))
        mNoteTag = extras.getString(NotesDbAdapter.KEY_TAGS);
      mFilename = extras.getString("Filename");
      // get title - only used for Tutorial doc
      if(extras.containsKey("TitleString"))
        mTitleString = extras.getString("TitleString");
    }
    else {
      mRowId = (Long) savedInstanceState.getSerializable(NotesDbAdapter.KEY_ROWID);
      // to handle the case of open doc w/o adding to DB ...
      if(mRowId != null && mRowId <= 0 && extras != null)
        mFilename = extras.getString("Filename");
      // what about sortOrder?
    }
    // handle import doc case
    if(extras != null && extras.getBoolean("importOnly", false)) {
      if(staticLoad)
        initHelper();
      // if finish is called from onCreate, onPause never gets called, so we must call saveState ourselves
      populateFields();
      saveState();
      finish();
      return;
    }

    initHelper();

    if(extras != null && extras.containsKey("imageURI")) {
      // doRepaint is necessary to pass canvas size to scribble so image gets pasted to middle ... but
      //  doesn't seem to work on first launch
      doRepaint();
      final Uri streamuri = (Uri)getIntent().getParcelableExtra("imageURI");
      doInsertImage(new ImageInputStreamFactory() {
        public InputStream getStream() throws FileNotFoundException {
          return getContentResolver().openInputStream(streamuri);
        }
      });
      // ensure image doesn't get repasted if we're restarted with the same intent, e.g., on screen rotation
      getIntent().removeExtra("imageURI");
    }
    // reopen webview
    if(savedInstanceState != null && savedInstanceState.getBoolean("showWebView", false)) {
      showWebView(true);
      webView.restoreState(savedInstanceState);
    }
    // timer event for auto scrolling
    mHandler = new Handler();
    mTimerTask = new Runnable() {
      public void run() {
        mHandler.postDelayed(this, 100);
        if(jniTimerEvent())
          doRepaint();
      }
    };
  }

  // split out so that this code can be called by first run of import
  private void initHelper()
  {
    // API level 14 introduces MotionEvent.getToolType
    apiLevel14 = (android.os.Build.VERSION.SDK_INT >= 14);
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    // dipWidth is used to control how many icons get put on action bar
    dipWidth = (int)(metrics.widthPixels/metrics.density);
    dipHeight = (int)(metrics.heightPixels/metrics.density);
    if(mPrefs.getFloat("preScale", -1) != metrics.density) {
      prefEditor.putFloat("preScale", metrics.density);
      prefEditor.apply();
    }
    setupPageSizes();

    if(mPrefs.getBoolean("KeepScreenOn", false))
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    // Fullscreen hides the actionbar, leaving no way to access menu in Android 3,4
    // fullscreen (hide android status bar)
    //requestWindowFeature(Window.FEATURE_NO_TITLE);
    //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    // to get a transparent actionbar over content:
    //getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
    myview = new MyView(this);
    setContentView(myview);
    // update config so that it's correct for the first doc
    prefsUpdated();
    populateFields();

    // instruct createActionBar to set the pen
    lastPen = new PenPreviewView(this);
    // restore prev pen if scribble has been reloaded
    lastPen.setPenProps(mPrefs.getString("lastPen", "0xFF000000,1,0,0"));
    if(staticLoad) {
      jniSetPen(lastPen.penColor, lastPen.penWidth, lastPen.penPressure, lastPen.penHighlight);
      staticLoad = false;
    }
  }

  @Override
  protected void onPause()
  {
    mHandler.removeCallbacks(mTimerTask);
    prefEditor.putString("lastPen", lastPen.getPenProps());
    prefEditor.apply();
    saveState();
    if(webView != null)
      webView.onPause();
    super.onPause();
  }

  @Override
  protected void onResume()
  {
    super.onResume();
    updateUI();
    if(webView != null)
      webView.onResume();
    // because there is only one instance of native library, but possibly multiple instances of FingerPaint
    //  we must ensure consistency; problem appears when opening files with 3rd party file manager
    // alternate approach is to set launchMode=singleTask to force single instance of FingerPaint, but this
    //  causes nonstandard behavior with activity history
    if(!jniCmpFilename(mFilename)) {
      mCancel = true;
      finish();
    }
    mHandler.postDelayed(mTimerTask, 100);
  }

  @Override
  protected void onDestroy()
  {
    if(mHandler != null)
      mHandler.removeCallbacks(mTimerTask);
    // don't know if we're actually exiting or being restarted due to orientation change, so
    //  don't close doc! ... jniItemClick(ID_NEWDOC);
    if(mDbHelper != null)
      mDbHelper.close();
    super.onDestroy();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putSerializable(NotesDbAdapter.KEY_ROWID, mRowId);
    outState.putBoolean("restarting", true);

    // in rare cases, it appears this can be called before mMenu is initialized
    if(webView != null && mMenu != null && mMenu.findItem(Menu.FIRST + ID_WEBVIEW).isChecked()) {
      webView.saveState(outState);
      outState.putBoolean("showWebView", true);
    }
  }

  protected void setupPageSizes()
  {
    if(!mPrefs.contains("screenPageWidth")) {
      Display display = getWindowManager().getDefaultDisplay();
      DisplayMetrics metrics = new DisplayMetrics();
      display.getMetrics(metrics);
      // depends on orientation and shit
      int rawwidth = metrics.widthPixels;
      int rawheight = metrics.heightPixels;
      try {
        Point rawsize = new Point(rawwidth, rawheight);
        // documented method added at API level 17
        Method mGetRealSize = Display.class.getMethod("getRealSize");
        mGetRealSize.invoke(display);
        rawwidth = rawsize.x;
        rawheight = rawsize.y;
      }
      catch(Exception e1) {
        try {
          // undocumented methods removed at API level 17
          Method mGetRawH = Display.class.getMethod("getRawHeight");
          Method mGetRawW = Display.class.getMethod("getRawWidth");
          rawwidth = (Integer) mGetRawW.invoke(display);
          rawheight = (Integer) mGetRawH.invoke(display);
        }
        catch(Exception e2) {}
      }
      int pagewidth = (int)(Math.min(rawwidth, rawheight)/metrics.density) - 24;
      int pageheight = (int)(Math.max(rawwidth, rawheight)/metrics.density) - 24;
      prefEditor.putInt("screenPageWidth", pagewidth);
      prefEditor.putInt("screenPageHeight", pageheight);
      if(!mPrefs.contains("pageWidth")) {
        prefEditor.putInt("pageWidth", pagewidth);
        prefEditor.putInt("pageHeight", pageheight);
        prefEditor.putInt("marginLeft", Math.min(100, pagewidth/7));
        // we'll just use scribble's defaults for ruling, color, etc
      }
      prefEditor.apply();
    }
  }

  // convert prefs to XML string to pass to C++ side
  protected String serializePrefs()
  {
    String prefsXML = "<?xml version='1.0' encoding='utf-8' standalone='yes' ?>\n<map>\n";
    Map<String,?> keys = mPrefs.getAll();
    for(Map.Entry<String,?> entry : keys.entrySet())
      prefsXML += String.format("<auto name='%s' value='%s'/>\n", entry.getKey(), entry.getValue().toString());
    prefsXML += "</map>\n";
    return prefsXML;
  }

  protected void prefsUpdated()
  {
    switch(PaintMain.atoi(mPrefs.getString("VolBtnMode", "1"), 1)) {
    case 0:  // none
      volUpAction = 0;
      volDownAction = 0;
      break;
    case 1:  // undo/redo
      volUpAction = ID_REDO;
      volDownAction = ID_UNDO;
      break;
    case 2: // zoom in/out
      volUpAction = ID_ZOOMIN;
      volDownAction = ID_ZOOMOUT;
      break;
    case 3:  // next/prev page
      volUpAction = ID_PREVPAGE;
      volDownAction = ID_NEXTPAGE;
      break;
    case 4:  // next/prev page (reversed)
      volUpAction = ID_NEXTPAGE;
      volDownAction = ID_PREVPAGE;
      break;
    case 5:
      volUpAction = ID_EMULATEPENBTN;
      volDownAction = ID_EMULATEPENBTN;
      break;
    }
    // load updated prefs
    myview.inputConfig();
    jniLoadGlobalConfig(serializePrefs());
  }

  private void populateFields()
  {
    String filename = mFilename;
    if(mRowId != null && mRowId > 0) {
      Cursor note = mDbHelper.fetchNote(mRowId);
      String body = null;
      if(note.getCount() > 0) {
        mTitleString = note.getString(note.getColumnIndexOrThrow(NotesDbAdapter.KEY_TITLE));
        body = note.getString(note.getColumnIndexOrThrow(NotesDbAdapter.KEY_BODY));
      }
      if(body != null && body.length() > 0 && body.charAt(0) == '\254')
        filename = body.substring(1);
      else {
        mCancel = true;
        fatalMessage(getString(R.string.db_error_msg));
      }
      note.close();
    }
    // don't reload file if we've been restarted, e.g., due to orientation change
    if(filename != null) {
      int res = jniOpenFile(filename);
      // either we can read file, or we can't ... no point in checking sdcard state
      if(res == -1) {
        mCancel = true;
        fatalMessage(getString(R.string.openerror_msg) + filename);
        // don't try to save anything on exit
      }
      else if(res != 0 && res != -3) {
        if(filename.endsWith(".svg"))
          alertMessage(getString(R.string.open_svg_msg));
        else
          alertMessage(getString(R.string.openerror_msg) + filename + "\n"
              + getString(R.string.missing_pages_msg));
      }
      mFilename = filename;
      if(mTitleString == null || mTitleString.isEmpty()) {
        String doctitle = jniGetConfigValue("docTitle");
        if(doctitle != null && !doctitle.isEmpty())
          mTitleString = doctitle;
        else
          mTitleString = (new File(filename)).getName();
      }
      if(mNoteTag == null)
        mNoteTag = jniGetConfigValue("docTags");
    }
    else {
      // create new document
      mFilename = new File(new File(getSavePath()), String.valueOf(System.currentTimeMillis()) + ".html").getPath();
      if(mTitleString == null || mTitleString.isEmpty())
        mTitleString = new SimpleDateFormat("MMM dd HH:mm").format(new Date());

      jniItemClick(ID_NEWDOC);
      if(!jniSetFilename(mFilename))
        fatalMessage(getString(R.string.fnerror_msg) + mFilename);
    }
    setTitle(mTitleString);
  }

  void saveState()
  {
    if(mCancel) {
      mCancel = false;
      return;
    }
    // save last URL open in webview
    if(mTitleString != null)
      jniSetDocConfigValue("docTitle", mTitleString);
    if(mNoteTag != null)
      jniSetDocConfigValue("docTags", mNoteTag);
    if(webView != null)
      jniSetDocConfigValue("lastURL", webView.getUrl());

    // error message won't actually show up if application is being paused!
    if(!jniSaveFile())
      alertMessage(getString(R.string.saveerror_msg) + mFilename);
    if(mRowId == null) {
      if(mFilename != null)
        mRowId = mDbHelper.createNote(0L, mTitleString, "\254" + mFilename, mNoteTag,
            null, new File(mFilename).lastModified());
      else
        mRowId = mDbHelper.createNote(0L, mTitleString, null, mNoteTag, null, 0);
      // subsequently file name must be obtained from DB
      //mFilename = null;
    }
    // rowId = -1 (!= null) is used to indicate document should not be added to DB
    if(mRowId > 0)
      updateDBRow();
  }

  private void updateDBRow()
  {
    // render a thumbnail
    try {
      int thumbw = THUMB_WIDTH;
      int thumbh = THUMB_HEIGHT;
      Bitmap thumb = Bitmap.createBitmap(thumbw, thumbh, Bitmap.Config.ARGB_8888);
      // Bitmap thumb = Bitmap.createScaledBitmap(mImg, thumbw, thumbh, true);
      jniPaintThumb(thumb);
      // arg to ByteArrayOutputStream is initial size in bytes ... size can grow ... so
      //  we're assuming 4x compression here
      ByteArrayOutputStream thumbout = new ByteArrayOutputStream(thumbw*thumbh);
      thumb.compress(Bitmap.CompressFormat.PNG, 100, thumbout);
      thumbout.flush();
      thumbout.close();
      byte[] thumbraw = thumbout.toByteArray();
      // do not modify filename column
      mDbHelper.updateNote(mRowId, mTitleString, null, mNoteTag, thumbraw);
    }
    catch(IOException e) {
      e.printStackTrace();
      fatalMessage(getString(R.string.dbuperr_msg));
    }
  }

  // these fns gets the folder path from a pref, or create the pref if it doesn't exist,
  //  and ensure that the path exists
  private String getSavePath()
  {
    String saveImgPath = mPrefs.getString("SaveImgPath", "");
    if(saveImgPath.isEmpty() || !(new File(saveImgPath).isDirectory())) {
      // set config value and create path
      saveImgPath = Environment.getExternalStorageDirectory().getPath() + getString(R.string.defaultdir);
      prefEditor.putString("SaveImgPath", saveImgPath);
      prefEditor.apply();
      new File(saveImgPath).mkdirs();
    }
    return saveImgPath;
  }

  private String getTempPath()
  {
    String tempPath;
    if(mPrefs.contains("tempPath"))
      tempPath = mPrefs.getString("tempPath", "");
    else {
      tempPath = Environment.getExternalStorageDirectory().getPath() + getString(R.string.tempdir);
      prefEditor.putString("tempPath", tempPath);
      prefEditor.apply();
    }
    new File(tempPath).mkdirs();
    return tempPath;
  }

  public void titleSet(String title)
  {
    mTitleString = title;
    setTitle(mTitleString);
  }

  private void doCancel()
  {
    mCancel = true;
    jniItemClick(ID_NEWDOC);
    //setResult(RESULT_CANCELLED); ???
    finish();
  }

  // cut and paste from file browser
  private void alertMessage(String msg)
  {
    new AlertDialog.Builder(this).setIcon(R.drawable.ic_dialog_alert)
        .setTitle(R.string.error)
        .setMessage(msg)
        .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {}
        }).show();
  }

  // cut and paste from file browser
  private void fatalMessage(String msg)
  {
    new AlertDialog.Builder(this).setIcon(R.drawable.ic_dialog_alert)
        .setTitle(R.string.error)
        .setCancelable(false)
        .setMessage(msg)
        .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) { finish(); }
        }).show();
  }

  private void confirmDiscardChanges()
  {
    new AlertDialog.Builder(this).setIcon(R.drawable.ic_dialog_alert)
    .setTitle(R.string.confirm_discard_title)
    .setMessage(R.string.confirm_discard_msg)
    .setPositiveButton(R.string.yes,
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) { doCancel(); }
      })
    .setNegativeButton(R.string.no,
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {}
      }).show();
  }

  // sending multiple attachments:
  // http://stackoverflow.com/questions/4552831/how-to-attach-multiple-files-to-email-client-in-android
  // Well, we can't send HTML mail or an embedded image with any of the built-in email clients, so looks
  //  like we'll need http://code.google.com/p/javamail-android/
  // Furthermore GMail will only display inline images attached to message and embedded with "cid:" - it strips out
  //  src attributes containing base64 data
  // I modified K-9 mail to enable this type of embedding, but I think it is too hacky to be a reasonable solution
  protected boolean sendBitmap()
  {
    int[] uiState = jniGetUIState();
    int pageNum = uiState[5];
    int totalPages = uiState[6];
    // sanitize filename by replacing everything but alphanumeric chars with "_"
    String title = getTitle().toString().replaceAll("\\W+", "_");
    String tempfilename = title + (totalPages > 1 ? String.format("_p%02d.png", pageNum) : ".png");
    String tempfilepath = getTempPath() + tempfilename;
    if(jniSavePNG(tempfilepath)) {
      final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
      emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      emailIntent.putExtra(Intent.EXTRA_SUBJECT, mTitleString);
      emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + tempfilepath));
      emailIntent.setType("image/png");
      startActivity(Intent.createChooser(emailIntent, getString(R.string.menu_sendpage)));
      return true;
    }
    return false;
  }

  // TODO: there is obviously a lot of duplicated code between sendBitmap and sendDocument
  protected boolean sendDocument()
  {
    String title = getTitle().toString().replaceAll("\\W+", "_");
    String tempfilepath = getTempPath() + title + ".html";
    if(jniSaveHTML(tempfilepath)) {
      final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
      emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      emailIntent.putExtra(Intent.EXTRA_SUBJECT, mTitleString);
      emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + tempfilepath));
      emailIntent.setType("text/html");
      startActivity(Intent.createChooser(emailIntent, getString(R.string.menu_senddoc)));
      return true;
    }
    return false;
  }

  public static void cleanUp()
  {
    // although this will consume more memory, it allows undo history to survive a trip to the home screen
    //jniItemClick(ID_NEWDOC);
  }

  private static final int THUMB_WIDTH = 375;
  private static final int THUMB_HEIGHT = 625;

  // we can optionally show a WebView so that user can take notes from a web page, video, etc
  // note that we don't create the WebView until it is first requested
  LinearLayout splitLayout = null;
  View browserView = null;
  WebView webView = null;
  EditText mEditURL = null;

  private class JetWebViewClient extends WebViewClient {
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon)
    {
      // update address bar - retain protocol unless it is plain http
      mEditURL.setText(url.replaceFirst("^http://", ""));
    }

    // prevent links from opening in separate, full-screen browser window
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
      // Use google docs viewer to open pdf and ppt; for now, don't bother with doc or xls
      // "embedded=false" is used to make sure the URL doesn't end with .pdf when we are called again
      if(url.endsWith(".pdf") || url.endsWith(".ppt") || url.endsWith(".pptx"))
        view.loadUrl("http://docs.google.com/viewer?url=" + URLEncoder.encode(url) + "&embedded=false");
      // return false to load URL in this webview
      return false;
    }
  }

  protected void showWebView(boolean show)
  {
    if(show) {
      // layout for children of LinearLayout ... looks like we don't
      //  have to change the layout for the LinearLayout itself (setLayoutParams)
      LinearLayout.LayoutParams geom = new LinearLayout.LayoutParams(
          LinearLayout.LayoutParams.MATCH_PARENT,
          LinearLayout.LayoutParams.MATCH_PARENT, 1);
      if(splitLayout == null) {
        createWebView();
        splitLayout = new LinearLayout(this);
        // For vertical split, WebView should be on top, so that it isn't obscured
        //  by hand when writing.  For horizontal split, WebView should be on left
        //  for right handed mode, on right for left-handed mode
        // We must replace myview as content view before we can add it to splitLayout
        setContentView(splitLayout);
        splitLayout.addView(myview, 0, geom);
      }
      // not sure if orientation can change without activity being restarted
      // also, doesn't appear possible to change order of Views in ViewGroup without
      //  removing and readding with different index
      Display display = getWindowManager().getDefaultDisplay();
      if(display.getWidth() > display.getHeight()) {
        splitLayout.setOrientation(LinearLayout.HORIZONTAL);
        int browseridx = mPrefs.getBoolean("leftHanded", false) ? 1 : 0;
        splitLayout.addView(browserView, browseridx, geom);
      }
      else {
        splitLayout.setOrientation(LinearLayout.VERTICAL);
        splitLayout.addView(browserView, 0, geom);
      }
      browserView.setVisibility(View.VISIBLE);
    }
    else if(browserView != null) {
      browserView.setVisibility(View.GONE);
      splitLayout.removeView(browserView);
    }
  }

  // We identify as a desktop browser to get flash video instead of HTML5, which is more
  //  work to show - see http://code.google.com/p/html5webview/
  protected void createWebView()
  {
    browserView = getLayoutInflater().inflate(R.layout.web_browser, null);
    mEditURL = (EditText)browserView.findViewById(R.id.editURL);
    Button goButton = (Button)browserView.findViewById(R.id.btnGo);
    ImageButton backButton = (ImageButton)browserView.findViewById(R.id.btnBack);
    ImageButton fwdButton = (ImageButton)browserView.findViewById(R.id.btnFwd);
    webView = (WebView)browserView.findViewById(R.id.webView);
    webView.setWebViewClient(new JetWebViewClient());
    // set default WebChromeClient to prevent crash when attempting fullscreen video
    // ICS WebView plays HTML5 video without the complicated custom WebChromeClient needed for Honeycomb
    webView.setWebChromeClient(new WebChromeClient());
    WebSettings cfg = webView.getSettings();
    cfg.setJavaScriptEnabled(true);
    // also need to set android:hardwareAccelerated="true" for flash to work
    cfg.setPluginState(PluginState.ON);
    // enable pinch zoom (and zoom buttons)
    cfg.setBuiltInZoomControls(true);
    // allow for zooming to less than 1x
    if(mPrefs.getBoolean("WideWebViewPort", true)) {
      cfg.setUseWideViewPort(true);
      cfg.setLoadWithOverviewMode(true);
    }
    // use a desktop User agent string to force youtube to play flash
    if(mPrefs.getBoolean("DesktopUserAgent", false))
      webView.getSettings().setUserAgentString("Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.13");
    // setup browser toolbar buttons
    goButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        String url = mEditURL.getText().toString().trim();
        webView.loadUrl(url.matches("^\\w+://.*") ? url : "http://" + url);
      }
    });
    backButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if(webView.canGoBack()) {
          webView.goBack();
        }
      }
    });
    fwdButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if(webView.canGoForward()) {
          webView.goForward();
        }
      }
    });
    // intercept enter on keyboard
    mEditURL.setOnEditorActionListener(new OnEditorActionListener() {
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_GO) {
          //InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
          //in.hideSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
          webView.loadUrl("http://" + mEditURL.getText().toString());
        }
        return false;
      }
    });
    String lastURL = jniGetConfigValue("lastURL");
    webView.loadUrl((lastURL != null && !lastURL.isEmpty()) ? lastURL : "http://www.google.com");
  }


  public class MyView extends SurfaceView implements SurfaceHolder.Callback
  {
    private final Context mContext;
    private Toast mToast;

    private int mPenType = 0;
    private static final int FORCED_PEN = -1;
    private static final int THINKPAD_PEN = 1;
    private static final int ICS_PEN = 2;
    private static final int SAMSUNG_PEN = 3;
    private static final int TEGRA_PEN = 4;
    // The original value of DETECTED_PEN was 10; everytime we make changes that allow for more specific
    //  pen detection, we should increment the value of DETECTED_PEN, which will result in detectPenType()
    //  being rerun once
    private static final int REDETECT_PEN = 10;
    private static final int DETECTED_PEN = 11;

    private long mTouchHoldoff = 0;
    private int sPenBtnMode = 0;
    private boolean penBtnPressed = false;

    // constructor
    public MyView(Context c)
    {
      super(c);
      mContext = c;
      // set callback for changes to view
      getHolder().addCallback(this);

      mPenType = mPrefs.getInt("PenType", 0);
      if(!mPrefs.contains("PenType") || (mPenType >= REDETECT_PEN && mPenType < DETECTED_PEN))
        detectPenType();
    }

    public void inputConfig()
    {
      // allow force pen to override detected pen type
      if(mPrefs.getBoolean("ForcePen", false))
        mPenType = FORCED_PEN;
      jniSetGlobalConfigValue("penAvailable", mPenType == 0 ? "0" : "1");

      mTouchHoldoff = PaintMain.atoi(mPrefs.getString("TouchHoldoff", "200"), 200);

      if(mPenType == SAMSUNG_PEN)
        sPenBtnMode = PaintMain.atoi(mPrefs.getString("penButtonMode", "17"), MODE_SELECT);
    }

    private void detectPenType()
    {
      // first, do generic pen detection
      if(mContext.getPackageManager().hasSystemFeature("android.hardware.touchscreen.pen")) {
        // Quill checks a list to determine if pressure sensitivity is available; any way to get this from OS?
        mPenType = ICS_PEN;
      }
      else if(mPenType >= REDETECT_PEN && mPenType < DETECTED_PEN)
        mPenType = DETECTED_PEN;

      // We could also iterate over InputDevice.getDeviceIds(), checking getDevice().getSources()
      // now try to detect specific devices
      String model = android.os.Build.MODEL;
      if(model.equalsIgnoreCase("ThinkPad Tablet")) {
        // Lenovo ThinkPad Tablet: pressure sensitive pen
        mPenType = THINKPAD_PEN;
      }
      else if(model.startsWith("GT-N") || model.startsWith("SM-P") || model.startsWith("SM-N")
          || (mPenType > 0 && (android.os.Build.BRAND.equalsIgnoreCase("Samsung")
          || android.os.Build.MANUFACTURER.equalsIgnoreCase("Samsung")))) {
        // international galaxy note is GT-N7000, note 2 is GT-N7100, Note 10.1 is GT-N8013
        // Note 10.1 2014 models are SM-P6xx
        // Note 3 phones are SM-N9xxx
        // US Note phones have a ton of different model numbers
        mPenType = SAMSUNG_PEN;
      }
      else if(model.startsWith("TegraNote")
          || mContext.getPackageManager().hasSystemFeature("com.nvidia.feature.DirectStylus")
          || mContext.getPackageManager().hasSystemFeature("com.nvidia.nvsi.feature.DirectStylus")
          || mContext.getPackageManager().hasSystemFeature("com.nvidia.nvsi.product.TegraNOTE7")) {
        // need to rescale pressure for TegraNote/DirectStylus
        mPenType = TEGRA_PEN;
      }
      //for(android.content.pm.FeatureInfo f : mContext.getPackageManager().getSystemAvailableFeatures())
      //if(f.name.contains("com.nvidia.nvsi.product.TegraNOTE"))
      //mPenType = TEGRA_PEN;

      prefEditor.putInt("PenType", mPenType);
      // enable pan from edge by default if no pen
      if(mPenType == 0 && !mPrefs.contains("panFromEdge"))
        prefEditor.putBoolean("panFromEdge", true);
      prefEditor.apply();
    }

    // SurfaceHolder.Callback interface
    public void surfaceCreated(SurfaceHolder holder) {}
    public void surfaceDestroyed(SurfaceHolder holder) {}
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
      jniItemClick(ID_DIRTYSCREEN);
      doRepaint();
    }

    // must match values defined in scribbleview.h
    private static final int MODEMOD_NONE = 0;
    private static final int MODEMOD_ERASE = 1;
    private static final int MODEMOD_PENBTN = 2;
    //private static final int MODEMOD_MOVESEL = 4;
    //private static final int MODEMOD_SCALESEL = 8;
    private static final int MODEMOD_EDGETOP = 16;
    //private static final int MODEMOD_EDGEBOTTOM = 32;
    //private static final int MODEMOD_EDGELEFT = 64;
    //private static final int MODEMOD_EDGERIGHT = 128;

    //private static final int INPUTSOURCE_NONE = 0;
    //private static final int INPUTSOURCE_MOUSE = 1;
    private static final int INPUTSOURCE_PEN = 2;
    private static final int INPUTSOURCE_TOUCH = 3;

    private static final int INPUTEVENT_RELEASE = -1;
    private static final int INPUTEVENT_MOVE = 0;
    private static final int INPUTEVENT_PRESS = 1;
    private static final int INPUTEVENT_CANCEL = 2;

    // Coordinate system
    // (0,0) ----> x
    // |
    // |
    // |
    // v y
    //

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
      return false;
    }

    private void inputEvent(MotionEvent event, int eventtype)
    {
      if(event.getPointerCount() > 1) {
        jniTwoInput(event.getX(0), event.getY(0), event.getX(1), event.getY(1), eventtype);
      }
      else {
        float ps = (mPenType == TEGRA_PEN) ? 3 : 1;
        if(eventtype == INPUTEVENT_MOVE) {
          int n = event.getHistorySize();
          for(int ii = 0; ii < n; ii++) {
            //android.util.Log.d("SingleInputHist", "X: " + event.getHistoricalX(ii) + ", Y: " + event.getHistoricalY(ii) + ", P: " + event.getHistoricalPressure(ii));
            jniSingleInput(event.getHistoricalX(ii),
                event.getHistoricalY(ii), event.getHistoricalPressure(ii) * ps, eventtype);
          }
        }
        //android.util.Log.d("SingleInput", "X: " + event.getX() + ", Y: " + event.getY() + ", P: " + event.getPressure());
        jniSingleInput(event.getX(), event.getY(), event.getPressure() * ps, eventtype);
      }
    }

    boolean ignoreTouch = false;
    boolean penEvent = false;
    long prevTouchExitTime = 0;
    long prevHoverExitTime = 0;

    // if we switch min api level to 14, replace this with onHoverEvent() (see Quill)
    @TargetApi(14)
    @Override
    public boolean onGenericMotionEvent(MotionEvent event)
    {
      if(apiLevel14) {
        if(event.getToolType(0) == MotionEvent.TOOL_TYPE_STYLUS) {
          switch (event.getAction()) {
          case MotionEvent.ACTION_HOVER_ENTER:
            if(event.getEventTime() - prevTouchExitTime < mTouchHoldoff)
              jniCancelLastPan();
            //Log.d("MotionEvent", "Hover Enter at " + event.getEventTime());
          case MotionEvent.ACTION_HOVER_MOVE:
            if(sPenBtnMode > 0) {
              boolean pressed = (event.getButtonState() & MotionEvent.BUTTON_SECONDARY) == MotionEvent.BUTTON_SECONDARY;
              if(pressed && !penBtnPressed) {
                if(mActiveMode != MODE_STROKE)
                  jniItemClick(MODE_STROKE);
                else
                  jniItemClick(sPenBtnMode);
                updateUI();
              }
              penBtnPressed = pressed;
            }
            return true;
          case MotionEvent.ACTION_HOVER_EXIT:
            prevHoverExitTime = event.getEventTime();
            //Log.d("MotionEvent", "Hover Exit at " + event.getEventTime());
            penBtnPressed = false;
            return true;
          default:
            break;
          }
        }
      }
      return super.onGenericMotionEvent(event);
    }

    // TODO: refactor the logic here - it's turned into a big mess!
    @TargetApi(14)
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
      //android.util.Log.d("MotionEvent", event.toString());
      if(mToast != null) {
        mToast.cancel();
        mToast = null;
      }

      if(ignoreTouch && event.getActionMasked() != MotionEvent.ACTION_DOWN)
        return true;

      switch(event.getActionMasked()) {
      case MotionEvent.ACTION_DOWN:
        int modemod = MODEMOD_NONE;
        penEvent = false;
        // Pen button on Samsung S-Pen unfortunately is not available because Samsung uses it to activate
        //  system-wide pen gestures
        if(apiLevel14) {
          int tooltype = event.getToolType(0);
          if(tooltype == MotionEvent.TOOL_TYPE_ERASER) {
            penEvent = true;
            modemod = MODEMOD_ERASE;
          }
          else {
            penEvent = (tooltype == MotionEvent.TOOL_TYPE_STYLUS);
            if((event.getButtonState() & MotionEvent.BUTTON_SECONDARY) == MotionEvent.BUTTON_SECONDARY)
              modemod = MODEMOD_PENBTN;
          }
        }
        // check for pen event ... previously we were checking this for every touch event
        if(!penEvent && mPenType != 0) {
          switch(mPenType) {
          case THINKPAD_PEN:
            penEvent = (event.getTouchMajor() == 0.0f);
            break;
          case ICS_PEN:
          case SAMSUNG_PEN:
            int SOURCE_STYLUS = 0x00004002; // InputDevice.SOURCE_STYLUS in ICS
            penEvent = ((event.getSource() & SOURCE_STYLUS) == SOURCE_STYLUS);
            break;
          }
        }
        else if(penEvent && mPenType == 0) {
          mPenType = DETECTED_PEN;
          detectPenType();
          prefsUpdated();
        }
        // going to have to make this configurable!
        ignoreTouch = !penEvent && event.getEventTime() - prevHoverExitTime < mTouchHoldoff;
        if(!ignoreTouch) {
          if((volUpAction == ID_EMULATEPENBTN && volUpState == KeyEvent.ACTION_DOWN)
              || (volDownAction == ID_EMULATEPENBTN && volDownState == KeyEvent.ACTION_DOWN)) {
            modemod = MODEMOD_PENBTN;
          }
          // getEdgeFlags() always returns zero (at least on TPT), so don't bother
          //modemod += event.getEdgeFlags() * MODEMOD_EDGETOP;
          int pressedmode = 0;
          if(mPressedToolBtn != null) {
            View v = mPressedToolBtn.getActiveView();
            mPressedToolBtn.disallowSelection();
            if(v != null && v.getTag() != null)
              pressedmode = ((Integer)v.getTag()).intValue();
          }
          jniInputStart(penEvent ? INPUTSOURCE_PEN : INPUTSOURCE_TOUCH, modemod, pressedmode);
          inputEvent(event, INPUTEVENT_PRESS);
        }
        break;
      case MotionEvent.ACTION_MOVE:
        inputEvent(event, INPUTEVENT_MOVE);
        break;
      case MotionEvent.ACTION_UP:
        if(!penEvent)
          prevTouchExitTime = event.getEventTime();
        inputEvent(event, INPUTEVENT_RELEASE);
        updateUI();
        break;
      case MotionEvent.ACTION_POINTER_DOWN:
        inputEvent(event, INPUTEVENT_PRESS);
        break;
      case MotionEvent.ACTION_POINTER_UP:
        inputEvent(event, INPUTEVENT_RELEASE);
        break;
      case MotionEvent.ACTION_CANCEL:
        jniItemClick(ID_CANCEL);
        //inputEvent(event, INPUTEVENT_CANCEL);
        //updateUI(); ???
        break;
      }
      doRepaint();
      return true;
    }
  } // end MyView class

  // ColorPickerDialog callback
  // OnToolChangedListener interface - Color.TRANSPARENT + i is
  // used to indicate special tools
  // TODO: consider including recent pen functionality as an alternative to explicit saving of pens
  public void toolChanged(int color, float width, float pressure, boolean highlight, int saveslot)
  {
    jniSetPen(color, width, pressure, highlight);
    lastPen.setPenProps(color, width, pressure, highlight);
    if(saveslot >= 0 && saveslot < penViews.length && penViews[saveslot] != null) {
      penViews[saveslot].setPenProps(color, width, pressure, highlight);
      prefEditor.putString("savedPen" + saveslot, penViews[saveslot].getPenProps());
      prefEditor.apply();
    }
    mCurrPenNum = saveslot;
  }

  protected void doToolSel()
  {
    int[] uiState = jniGetUIState();
    // uiState[8] is pen width * 100, uiState[9] is penColor, uiState[12] is
    //  userMode ... we hide width selector if setting bookmark color
    float width = -1;
    if(uiState[12] != MODE_BOOKMARK) {
      width = uiState[8]/100.0f;
      doJNIAction(MODE_STROKE);
    }
    if(uiState[2] != 0)  // active selection
      new ToolPickerDialog(this, this, uiState[9], width, 0, false).show();
    else
      new ToolPickerDialog(this, this,
          lastPen.penColor, lastPen.penWidth, lastPen.penPressure, lastPen.penHighlight).show();
  }

  // PageSetupDialog callback
  public void pageSetup(String s)
  {
    boolean clipped = jniSetPageProps(s);
    jniLoadGlobalConfig(serializePrefs()); // in case defaults were set
    doRepaint();
    updateUI();

    if(clipped) {
      new AlertDialog.Builder(this).setIcon(R.drawable.ic_dialog_alert)
      .setTitle(R.string.pagesize_title)
      .setMessage(R.string.pagesize_msg)
      .setPositiveButton(R.string.yes,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) { }
        })
      .setNegativeButton(R.string.no,
        new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) { jniItemClick(ID_UNDO);  doRepaint(); }
        }).show();
    }
  }

  protected void doPageSetup()
  {
    new PageSetupDialog(this, this, mPrefs, getString(R.string.menu_pagesetup), jniGetPageProps()).show();
  }

  protected void doRepaint()
  {
    Surface s = myview.getHolder().getSurface();
    if(s.isValid())
      jniPaint(s);
  }

  // must agree with values on C++ side
  public static final int MODE_NONE = 10;
  public static final int MODE_PAN = 11;
  public static final int MODE_STROKE = 12;
  public static final int MODE_ERASE = 13;
  public static final int MODE_ERASESTROKE = 14;
  public static final int MODE_ERASERULED = 15;
  public static final int MODE_ERASEFREE = 16;
  public static final int MODE_SELECT = 17;
  public static final int MODE_SELECTRECT = 18;
  public static final int MODE_SELECTRULED = 19;
  public static final int MODE_SELECTLASSO = 20;
  public static final int MODE_MOVESEL = 21;
  public static final int MODE_MOVESELFREE = 22;
  public static final int MODE_MOVESELRULED = 23;
  public static final int MODE_INSSPACE = 24;
  public static final int MODE_INSSPACEVERT = 25;
  public static final int MODE_INSSPACEHORZ = 26;
  public static final int MODE_INSSPACERULED = 27;
  public static final int MODE_BOOKMARK = 28;
  public static final int MODE_LAST = 29;

  //actions
  private static final int ID_UNDO = 100;
  private static final int ID_REDO = 101;
  private static final int ID_SELALL = 102;
  private static final int ID_SELSIMILAR = 103;
  private static final int ID_INVSEL = 104;
  private static final int ID_DELSEL = 105;
  private static final int ID_COPYSEL = 106;
  private static final int ID_CUTSEL = 107;
  private static final int ID_PASTE = 108;
  private static final int ID_CANCEL = 109;
  private static final int ID_EXPANDDOWN = 111;
  private static final int ID_EXPANDRIGHT = 112;
  private static final int ID_ZOOMIN = 113;
  private static final int ID_ZOOMOUT = 114;
  private static final int ID_RESETZOOM = 115;
  private static final int ID_PREVPAGE = 116;
  private static final int ID_NEXTPAGE = 117;
  private static final int ID_PAGEAFTER = 118;
  private static final int ID_PAGEBEFORE = 119;
  private static final int ID_DELPAGE = 120;
  private static final int ID_NEWDOC = 121;
  private static final int ID_ZOOMALL = 123;
  private static final int ID_ZOOMWIDTH = 124;
  private static final int ID_DUPSEL = 130;
  private static final int ID_LINKBOOKMARK = 131;
  private static final int ID_TOGGLEBOOKMARKS = 510;
  private static final int ID_DIRTYSCREEN = 511;

  // not passed thru JNI
  private static final int ID_SETPEN = 1000;
  private static final int ID_SAVE = 1001;
  private static final int ID_PAGESETUP = 1002;
  private static final int ID_PREFERENCES = 1003;
  private static final int ID_SENDPAGE = 1004;
  private static final int ID_DISCARD = 1005;
  private static final int ID_WEBVIEW = 1006;
  private static final int ID_TOOLMENU = 1007;
  private static final int ID_SENDDOC = 1008;
  private static final int ID_EXPORTDOC = 1009;
  private static final int ID_RENAME = 1010;
  private static final int ID_EXPORTPDF = 1011;
  private static final int ID_LINKSEL = 1012;
  private static final int ID_SELECTIMG = 1020;
  private static final int ID_CAPTUREIMG = 1021;
  private static final int ID_GETIMG = 1022;  // combined chooser for camera + gallery

  private static final int ID_SAVEDPEN = 2000;
  private static final int ID_EMULATEPENBTN = 3000;

  // icon background colors
  private static final int TOOL_NOUSE = 0x00000000;
  private static final int TOOL_ONEUSE = 0x5F0000FF;
  private static final int TOOL_LOCKED = 0xBF0000FF;

  private PenPreviewView penViews[];
  private final String defaultPens[] = {"0xFF000000", "0xFFFF0000", "0xFF00FF00",
                                        "0xFF0000FF", "0xFFFFFF00", "0xFF444444"};
  private int mCurrPenNum = -1;

  private MenuItem addMenuItem(Menu menu, int id, int titleid, int icon)
  {
    return addMenuItem(menu, id, titleid, icon, MenuItem.SHOW_AS_ACTION_NEVER, Menu.NONE);
  }

  private MenuItem addMenuItem(Menu menu, int id, int titleid, int icon, int action, int group)
  {
    MenuItem item = menu.add(group, Menu.FIRST + id, 0, titleid);
    item.setShowAsAction(action);
    if(icon != 0)
      item.setIcon(icon);
    return item;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    //if(isFinishing()) return true;
    mMenu = menu;
    //MenuInflater inflater = getMenuInflater();
    //inflater.inflate(R.menu.fingerpaint_menu, menu);
    //int dispwidth = getWindowManager().getDefaultDisplay().getWidth();
    int showaction = MenuItem.SHOW_AS_ACTION_IF_ROOM;  //MenuItem.SHOW_AS_ACTION_ALWAYS;
    createActionBar();

    if(dipWidth <= 600)
      showaction = MenuItem.SHOW_AS_ACTION_NEVER;
    addMenuItem(menu, ID_TOGGLEBOOKMARKS, R.string.menu_bookmarks, R.drawable.ic_menu_bookmark, showaction, Menu.NONE).setCheckable(true);
    addMenuItem(menu, ID_PREVPAGE, R.string.menu_prevpage, R.drawable.ic_menu_up, showaction, Menu.NONE);
    addMenuItem(menu, ID_NEXTPAGE, R.string.menu_nextpage, R.drawable.ic_menu_down, showaction, Menu.NONE);
    // these are never on the action bar; note that icons do not appear on main menu, but will appear on
    //  submenus
    // Selection submenu .. some items may be on action bar

    SubMenu selmenu = menu.addSubMenu(getString(R.string.menu_selection) + "...");
    if(dipWidth <= 480) {
      addMenuItem(selmenu, ID_CUTSEL, R.string.menu_cut, R.drawable.ic_menu_cut);
      addMenuItem(selmenu, ID_COPYSEL, R.string.menu_copy, R.drawable.ic_menu_copy);
      addMenuItem(selmenu, ID_PASTE, R.string.menu_paste, R.drawable.ic_menu_paste);
      addMenuItem(selmenu, ID_DUPSEL, R.string.menu_duplicate, R.drawable.ic_menu_copy);
      addMenuItem(selmenu, ID_DELSEL, R.string.menu_deletesel, R.drawable.ic_menu_discard);
      addMenuItem(selmenu, ID_SELALL, R.string.menu_selectall, R.drawable.ic_menu_select);
    }
    addMenuItem(selmenu, ID_LINKSEL, R.string.menu_linktoweb, R.drawable.ic_menu_globe);
    addMenuItem(selmenu, ID_LINKBOOKMARK, R.string.menu_linktobookmark, R.drawable.ic_menu_bookmark);
    addMenuItem(selmenu, ID_SELSIMILAR, R.string.menu_selsimilar, R.drawable.ic_menu_select);
    addMenuItem(selmenu, ID_INVSEL, R.string.menu_invsel, R.drawable.ic_menu_select);

    // Document menu
    SubMenu docmenu = menu.addSubMenu(getString(R.string.menu_document) + "...");
    addMenuItem(docmenu, ID_SENDDOC, R.string.menu_senddoc, 0);  /// R.drawable.ic_menu_send
    addMenuItem(docmenu, ID_SENDPAGE, R.string.menu_sendpage, 0); // or ic_menu_compose
    addMenuItem(docmenu, ID_EXPORTDOC, R.string.menu_exportdoc, 0);
    addMenuItem(docmenu, ID_EXPORTPDF, R.string.menu_exportpdf, 0);
    // Save Now has caused confusion and is unnecessary; just cycle display to force save
    //addMenuItem(docmenu, ID_SAVE, "Save Now", 0);
    addMenuItem(docmenu, ID_RENAME, R.string.menu_rename, 0);  // R.drawable.ic_menu_info
    addMenuItem(docmenu, ID_DISCARD, R.string.menu_discard, 0);
    // people seem to be having trouble finding Page Setup, so promote to main menu
    addMenuItem(menu, ID_PAGESETUP, R.string.menu_pagesetup, 0);
    // Page submenu
    SubMenu pagemenu = menu.addSubMenu(getString(R.string.menu_page) + "...");
    addMenuItem(pagemenu, ID_PAGEAFTER, R.string.menu_newpageafter, 0);
    addMenuItem(pagemenu, ID_PAGEBEFORE, R.string.menu_newpagebefore, 0);
    addMenuItem(pagemenu, ID_DELPAGE, R.string.menu_deletepage, 0);
    addMenuItem(pagemenu, ID_EXPANDDOWN, R.string.menu_expanddown, 0);
    addMenuItem(pagemenu, ID_EXPANDRIGHT, R.string.menu_expandright, 0);
    // Zoom submenu
    SubMenu zoommenu = menu.addSubMenu(getString(R.string.menu_zoom) + "...");
    addMenuItem(zoommenu, ID_ZOOMIN, R.string.menu_zoomin, 0);
    addMenuItem(zoommenu, ID_ZOOMOUT, R.string.menu_zoomout, 0);
    addMenuItem(zoommenu, ID_ZOOMALL, R.string.menu_zoompage, 0);
    addMenuItem(zoommenu, ID_ZOOMWIDTH, R.string.menu_zoomwidth, 0);
    addMenuItem(zoommenu, ID_RESETZOOM, R.string.menu_resetzoom, 0);
    // Insert image
    addMenuItem(menu, ID_GETIMG, R.string.menu_insert, R.drawable.ic_menu_add_pic);
    // Insert Image submenu
    //SubMenu insmenu = menu.addSubMenu(getString(R.string.menu_insert) + "...");
    //addMenuItem(insmenu, ID_CAPTUREIMG, R.string.menu_captureimg, R.drawable.ic_menu_camera);
    //addMenuItem(insmenu, ID_SELECTIMG, R.string.menu_selectimg, R.drawable.ic_menu_add_pic);
    // webview and prefs are expected to be accessed infrequently
    addMenuItem(menu, ID_WEBVIEW, R.string.menu_webview, 0).setCheckable(true);
    addMenuItem(menu, ID_PREFERENCES, R.string.menu_prefs, 0);
    //super.onCreateOptionsMenu(menu);
    // ensure that tool button updates properly
    mModeLocked = false;
    updateUI();
    return true;
  }

  private void enableMenuItem(int id, boolean enabled)
  {
    MenuItem item = mMenu.findItem(Menu.FIRST + id);
    if(item != null)
      item.setEnabled(enabled);
  }

  // TODO:
  // 1. improved drawing of saved pens; draw checkerboard background (use colorpicker AlphaPatternDrawable!)
  // 1. persisting selected tools
  // 1. migrations (i.e., have a most recent version executed pref ... if less than current version, run
  //  some code ... in this case to fix the pen button mode)

  private class UndoToolButtonListener implements UndoToolButton.UndoRedoListener
  {
    // Note that when using the undo wheel, we want to avoid the potentially slow UI update until finished
    public void undo()
    {
      jniItemClick(ID_UNDO);
      doRepaint();
    }

    public void redo()
    {
      jniItemClick(ID_REDO);
      doRepaint();
    }

    public boolean canUndo()
    {
      int[] uiState = jniGetUIState();
      return (uiState[0] != 0);
    }

    public boolean canRedo()
    {
      int[] uiState = jniGetUIState();
      return (uiState[1] != 0);
    }

    public void undoRedoEnd(boolean doUndo)
    {
      if(doUndo)
        jniItemClick(ID_UNDO);
      doRepaint();
      updateUI();
    }
  }

  private ToolButton mPressedToolBtn = null;

  public void viewPressed(ToolButton toolbtn, boolean down, boolean forceupdate)
  {
    if(down) {
      //myview.playSoundEffect(android.view.SoundEffectConstants.CLICK);
      findViewById(android.R.id.content).playSoundEffect(android.view.SoundEffectConstants.CLICK);
      mPressedToolBtn = toolbtn;
    }
    else
      mPressedToolBtn = null;
    // force update of active button
    if(forceupdate) {
      mModeLocked = false;
      updateUI();
    }
  }

  public void viewSelected(ToolButton toolbtn, View v)
  {
    if(v == null || v.getTag() == null)
      return;
    int tag = ((Integer)v.getTag()).intValue();
    if(tag >= ID_SAVEDPEN && tag < ID_SAVEDPEN + penViews.length) {
      int pennum = tag - ID_SAVEDPEN;
      // we must change mode before setting color so that we don't change bookmark color
      doJNIAction(MODE_STROKE);
      jniSetPen(penViews[pennum].penColor, penViews[pennum].penWidth,
          penViews[pennum].penPressure, penViews[pennum].penHighlight);
      lastPen.setPenProps(penViews[pennum].penColor, penViews[pennum].penWidth,
          penViews[pennum].penPressure, penViews[pennum].penHighlight);
      mCurrPenNum = pennum;
      mModeLocked = false;
      if(toolbtn != null)
        toolbtn.setActive(2, pennum + ID_SAVEDPEN);
      return;
    }
    switch (tag) {
    case ID_SETPEN:
      doToolSel();
      if(toolbtn != null)
        toolbtn.setActive(2, -1);
      break;
    case MODE_STROKE:
      if(toolbtn != null)
        toolbtn.setActive(2, mCurrPenNum >= 0 ? mCurrPenNum + ID_SAVEDPEN : -1);
    default:
      mModeLocked = false;  // force refresh of action bar button
      doJNIAction(tag);
    }
  }

  // another option would be to use the standard action bar view, but set android:actionViewClass to
  //  our drop down menu creator (equiv to using setActionView())
  // TODO: how to add separators?
  // Current situation:
  // 1. ActionBar.setCustomView: doesn't work on Galaxy Note emulator (just not enough room!)
  //  w/ split action bar: custom view is shown at top ... only overflow menu icon at bottom
  // 1. MenuItem.setActionView(): items too closely spaced; no other items are shown, even when there is lots of room
  //  w/ split action bar enabled - action bar items equally spaced along bottom action bar, so spacing is not
  //  a problem ... but undo will have to be in the middle or something.  Also would need to figure out how to
  //  show PopupWindow above toolbar!

  private ToolButton panButton;
  private ToolButton drawButton;
  private ToolButton eraseButton;
  private ToolButton selectButton;
  private ToolButton insSpaceButton;
  private UndoToolButton undoButton;
  private ToolButton clipboardButton;

  @TargetApi(14)
  private void createActionBar()
  {
    ActionBar actionBar = getActionBar();
    LinearLayout actionBarView = (LinearLayout) getLayoutInflater().inflate(R.layout.action_bar, null);
    boolean pressOpen = mPrefs.getBoolean("PressOpenMenus", true);

    panButton = (ToolButton) actionBarView.findViewById(R.id.action_pan);
    panButton.setTag(MODE_PAN);
    panButton.setListener(this, false);
    if(!mPrefs.getBoolean("ShowPanTool", false))
      actionBarView.removeView(panButton);

    clipboardButton = (ToolButton) actionBarView.findViewById(R.id.action_clipboard);
    clipboardButton.addMenuItem(R.drawable.ic_menu_cut, R.string.menu_cut, ID_CUTSEL);
    clipboardButton.addMenuItem(R.drawable.ic_menu_copy, R.string.menu_copy, ID_COPYSEL);
    clipboardButton.addMenuItem(R.drawable.ic_menu_paste, R.string.menu_paste, ID_PASTE);
    clipboardButton.addMenuItem(R.drawable.ic_menu_copy, R.string.menu_duplicate, ID_DUPSEL);
    // use "Delete Selection" instead of "Delete" to make all menu items wider and thus easier to hit
    clipboardButton.addMenuItem(R.drawable.ic_menu_discard, R.string.menu_deletesel, ID_DELSEL);
    clipboardButton.addMenuItem(R.drawable.ic_menu_select, R.string.menu_selectall, ID_SELALL);
    // note no tag for clipboard button itself
    clipboardButton.setListener(this, pressOpen);
    if(dipWidth <= 480)
      actionBarView.removeView(clipboardButton);

    drawButton = (ToolButton) actionBarView.findViewById(R.id.action_draw);
    // TODO: preference for numPenPreviews
    penViews = new PenPreviewView[defaultPens.length];
    int numpens = defaultPens.length;
    if(dipHeight < 360)
      numpens = 3;
    else if(dipHeight <= 480)
      numpens = 4;
    for(int ii = 0; ii < numpens; ii++) {
      penViews[ii] = drawButton.addPenPreview(ID_SAVEDPEN + ii);
      // default width and pressure sensitivity depends on device
      String penwidth = mPrefs.getInt("PenType", 0) == MyView.TEGRA_PEN ? ",1.6,1,0" : ",1,0,0";
      // handle legacy saved pens
      // TODO: remove after next update
      if(mPrefs.contains("penColor" + ii)) {
        if(!mPrefs.contains("savedPen" + ii)) {
          penViews[ii].setPenProps(mPrefs.getInt("penColor" + ii, Color.BLACK),
              mPrefs.getFloat("penWidth" + ii, 1.0f), 0.0f, false);
          prefEditor.putString("savedPen" + ii, penViews[ii].getPenProps());
        }
        prefEditor.remove("penColor" + ii);
        prefEditor.remove("penWidth" + ii);
        prefEditor.apply();
      }
      // load saved pen properties
      penViews[ii].setPenProps(mPrefs.getString("savedPen" + ii, defaultPens[ii] + penwidth));
    }
    // init lastPen if first start
    if(!mPrefs.contains("lastPen")) {
      lastPen.setPenProps(penViews[0].getPenProps());
      jniSetPen(lastPen.penColor, lastPen.penWidth, lastPen.penPressure, lastPen.penHighlight);
    }

    drawButton.addMenuItem(R.drawable.ic_menu_set_pen, R.string.menu_custompen, ID_SETPEN);
    drawButton.addMenuItem(R.drawable.ic_menu_add_bookmark, R.string.menu_addbookmark, MODE_BOOKMARK);
    drawButton.setTag(MODE_STROKE);
    drawButton.setListener(this, pressOpen);

    eraseButton = (ToolButton) actionBarView.findViewById(R.id.action_erase);
    // inputs needed to create menu: icon, name, id
    eraseButton.addMenuItem(R.drawable.ic_menu_erase, R.string.menu_strokeeraser, MODE_ERASESTROKE);
    eraseButton.addMenuItem(R.drawable.ic_menu_erase_ruled, R.string.menu_rulederaser, MODE_ERASERULED);
    eraseButton.addMenuItem(R.drawable.ic_menu_erase_free, R.string.menu_freeeraser, MODE_ERASEFREE);
    eraseButton.setTag(MODE_ERASE);
    eraseButton.setListener(this, pressOpen);
    // hack to get mode from ScribbleArea
    jniItemClick(MODE_ERASE);
    eraseButton.setActive(0, jniGetUIState()[12]);

    selectButton = (ToolButton) actionBarView.findViewById(R.id.action_select);
    // inputs needed to create menu: icon, name, id
    selectButton.addMenuItem(R.drawable.ic_menu_select, R.string.menu_rectsel, MODE_SELECTRECT);
    selectButton.addMenuItem(R.drawable.ic_menu_select_ruled, R.string.menu_ruledsel, MODE_SELECTRULED);
    selectButton.addMenuItem(R.drawable.ic_menu_lasso_select, R.string.menu_lassosel, MODE_SELECTLASSO);
    selectButton.setTag(MODE_SELECT);
    selectButton.setListener(this, pressOpen);
    jniItemClick(MODE_SELECT);
    selectButton.setActive(0, jniGetUIState()[12]);

    insSpaceButton = (ToolButton) actionBarView.findViewById(R.id.action_insert_space);
    // inputs needed to create menu: icon, name, id
    insSpaceButton.addMenuItem(R.drawable.ic_menu_insert_space, R.string.menu_insspace, MODE_INSSPACEVERT);
    insSpaceButton.addMenuItem(R.drawable.ic_menu_insert_space_ruled, R.string.menu_ruledinsspace, MODE_INSSPACERULED);
    insSpaceButton.setTag(MODE_INSSPACE);
    insSpaceButton.setListener(this, pressOpen);
    jniItemClick(MODE_INSSPACE);
    insSpaceButton.setActive(0, jniGetUIState()[12]);
    // restore stroke mode
    jniItemClick(MODE_STROKE);

    undoButton = (UndoToolButton) actionBarView.findViewById(R.id.action_undo);
    undoButton.setListener(new UndoToolButtonListener());
    undoButton.setDialSteps(PaintMain.atoi(mPrefs.getString("undoCircleSteps", "36"), 36));

    ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.RIGHT);
    actionBar.setCustomView(actionBarView, lp);
    if(dipWidth <= 480)
      actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    else
      actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
    if(apiLevel14)
      actionBar.setHomeButtonEnabled(true);

    // ensure that motion events can be split between action bar and content (the default until Android 4.2 -
    //  see https://android.googlesource.com/platform/frameworks/base/+/refs/heads/master/core/res/res/layout/screen_action_bar.xml )
    try {
      // getRootView() and getDecorView() go one step too high in view hierarchy
      ViewGroup container = (ViewGroup) findViewById(android.R.id.content).getParent();
      container.setMotionEventSplittingEnabled(true);
    }
    catch(Exception e) {
      android.util.Log.d("Exception", "failed to set motion event splitting enabled!");
    }
  }

  private boolean mBookmarks = false;
  private boolean mModeLocked = false;
  private boolean mAppendPage = false;
  private int mActiveMode = -1;

  protected void updateUI()
  {
    if(mMenu == null)
      return;

    int[] uiState = jniGetUIState();
    // read values from
    //boolean undo = uiState[0] != 0;
    //boolean redo = uiState[1] != 0;
    boolean activeSel = uiState[2] != 0;
    boolean clipboard = uiState[3] != 0;
    //boolean released = uiState[4] != 0;
    int pageNum = uiState[5];
    int totalPages = uiState[6];
    //int zoom = uiState[7];
    // skipped: pen width, color, etc
    mBookmarks = uiState[11] != 0;
    // 12 is userMode, 13 is ruled mode
    int mode = uiState[12];
    int nextmode = uiState[13];
    boolean linkclicked = uiState[14] != 0;

    // might have to change icon to get disabled look in Android 3.0+
    // ui->actionSave->setEnabled(docmodified);
    //enableMenuItem(ID_UNDO, undo);
    //enableMenuItem(ID_REDO, redo);
    enableMenuItem(ID_CUTSEL, activeSel);
    enableMenuItem(ID_COPYSEL, activeSel);
    enableMenuItem(ID_DELSEL, activeSel);
    enableMenuItem(ID_DUPSEL, activeSel);
    enableMenuItem(ID_SELSIMILAR, activeSel);
    enableMenuItem(ID_INVSEL, activeSel);
    enableMenuItem(ID_LINKBOOKMARK, activeSel);
    enableMenuItem(ID_LINKSEL, activeSel);
    enableMenuItem(ID_PASTE, clipboard);
    // any change for show/hide bookmarks?
    enableMenuItem(ID_PREVPAGE, pageNum > 1);
    MenuItem bookmarkItem = mMenu.findItem(Menu.FIRST + ID_TOGGLEBOOKMARKS);
    if(mBookmarks != bookmarkItem.isChecked()) {
      //toggleIcon(bookmarkItem, mBookmarks ? TOOL_LOCKED : TOOL_NOUSE);
      bookmarkItem.setChecked(mBookmarks);
    }
    // next page button
    if(mAppendPage != (pageNum == totalPages)) {
      MenuItem nextPageItem = mMenu.findItem(Menu.FIRST + ID_NEXTPAGE);
      mAppendPage = pageNum == totalPages;
      nextPageItem.setIcon(mAppendPage ? R.drawable.ic_menu_append_page : R.drawable.ic_menu_down);
    }

    // this is only really necessary for when WebView is redisplayed after orientation change
    // so in theory, we could move this code to onCreateOptionsMenu
    MenuItem webViewItem = mMenu.findItem(Menu.FIRST + ID_WEBVIEW);
    boolean webviewvis = (browserView != null && browserView.getVisibility() == View.VISIBLE);
    if(webviewvis != webViewItem.isChecked()) {
      webViewItem.setChecked(webviewvis);
    }

    if(mActiveMode != mode || !mModeLocked) {
      ToolButton oldbtn = modeToButtonIdx(mActiveMode);
      if(oldbtn != null)
        oldbtn.setActive(0);
      ToolButton newbtn = modeToButtonIdx(mode);
      if(newbtn != null) {
        if(mode == MODE_STROKE) {
          drawButton.setImageResource(R.drawable.ic_menu_draw);
          newbtn.setActive(2, mCurrPenNum >= 0 ? mCurrPenNum + ID_SAVEDPEN : -1);
        }
        else
          newbtn.setActive((mode == nextmode) ? 2 : 1, mode);
      }
      // if the mode isn't locked, next press on button could change its state
      mActiveMode = mode;
      mModeLocked = (mode == nextmode);
    }

    if(linkclicked) {
      if(browserView == null || browserView.getVisibility() != View.VISIBLE) {
        showWebView(true);
        MenuItem item = mMenu.findItem(Menu.FIRST + ID_WEBVIEW);
        if(item != null)
          item.setChecked(true);
      }
      String url = jniGetClickedLink();
      webView.loadUrl(url.matches("^\\w+://.*") ? url : "http://" + url);
    }
  }

  // UI code is always in flux, so there is no point going out of the way to make it elegant or general
  private ToolButton modeToButtonIdx(int mode)
  {
   switch(mode) {
   case MODE_PAN:
     return panButton;
   case MODE_STROKE:
     return drawButton;
   case MODE_ERASESTROKE:
   case MODE_ERASERULED:
   case MODE_ERASEFREE:
     return eraseButton;
   case MODE_SELECTRECT:
   case MODE_SELECTRULED:
   case MODE_SELECTLASSO:
     return selectButton;
   case MODE_MOVESELFREE:
   case MODE_MOVESELRULED:
     return null;
   case MODE_INSSPACERULED:
   case MODE_INSSPACEVERT:
   case MODE_INSSPACEHORZ:
     return insSpaceButton;
   case MODE_BOOKMARK:
     return drawButton;
   }
   return null; // to suppress compiler warning
  }

  //private static final int ACTIVITY_SAVEHTML = 7770;
  //private static final int ACTIVITY_SAVEPDF = 7771;
  //private static final int ACTIVITY_EXPORTFILE = 7771;
  //private static final int ACTIVITY_PREFS = 7772;

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    int itemid = item.getItemId() - Menu.FIRST;
    if(itemid < 0)
      return true;

    switch(itemid) {
    case (android.R.id.home - Menu.FIRST):
      finish();
      return true;
    case ID_DISCARD:
      confirmDiscardChanges();
      return true;
    case ID_WEBVIEW:
      item.setChecked(!item.isChecked());
      showWebView(item.isChecked());
      break;
    case ID_PREFERENCES:
      // open settings activity
      startActivityForResult(new Intent(this, PaintPreferences.class), ID_PREFERENCES);
      break;
    case ID_SENDPAGE:
      sendBitmap();
      break;
    case ID_SENDDOC:
      sendDocument();
      break;
    case ID_PAGESETUP:
      doPageSetup();  // open page setup dialog
      break;
    case ID_SETPEN:
      doToolSel();
      return true;
    case ID_SAVE:
      saveState();
      return true;
    case ID_RENAME:
      new EditTextDialog(this, this, getString(R.string.edit_title), mTitleString).show();
      return true;
    case ID_LINKSEL:
      linkSelection();
      return true;
    case ID_EXPORTDOC:
    case ID_EXPORTPDF:
      Intent intent = new Intent(this, FileBrowser.class);
      intent.putExtra(FileBrowser.START_PATH, mPrefs.getString("ExportFilePath", "/sdcard"));
      intent.putExtra(FileBrowser.MODE, FileBrowser.MODE_SAVE);
      String exportfile = getTitle().toString().replaceAll("\\W+", "_");
      intent.putExtra(FileBrowser.START_NAME, exportfile + (itemid == ID_EXPORTDOC ? ".html" : ".pdf"));
      startActivityForResult(intent, itemid); // == ID_EXPORTDOC ? ACTIVITY_SAVEHTML : ACTIVITY_SAVEPDF);
      return true;
    case ID_SELECTIMG:
      intent = new Intent(Intent.ACTION_GET_CONTENT);
      intent.setType("image/*");
      //intent.setAction(Intent.ACTION_GET_CONTENT);
      startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), ID_SELECTIMG);
      /* intent = new Intent();
      intent.setAction(Intent.ACTION_PICK);
      intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
      startActivityForResult(intent, ID_SELECTIMG); */
      return true;
    case ID_CAPTUREIMG:
      intent = new Intent();
      intent.setAction("android.media.action.IMAGE_CAPTURE");
      intent.putExtra(MediaStore.EXTRA_OUTPUT,
          Uri.fromFile(new File(getTempPath(), "_camera.jpg")));
      startActivityForResult(intent, ID_CAPTUREIMG);
      return true;
    case ID_GETIMG:
      combinedGetImage();
    default:
      // all other menu item clicks are passed thru JNI
      doJNIAction(itemid);
    }
    return true;
  }

  private void linkSelection()
  {
    String href = jniGetHyperRef();
    if(href.length() < 1 && webView != null)
      href = webView.getUrl();
    new EditTextDialog(this, new EditTextDialog.OnTitleSetListener() {
      public void titleSet(String title) {
        jniCreateHyperRef(title);
      }
    }, getString(R.string.set_link_target), href).show();
  }

  private void doJNIAction(int id)
  {
    jniItemClick(id);
    doRepaint();
    updateUI();
  }

  // pasting from clipboard:
  //  see http://developer.android.com/guide/topics/text/copy-paste.html
  // See pre-Oct 2013 commits for ugly method of getting filename for image selected from gallery
  private void doInsertImage(ImageInputStreamFactory streamfactory)
  {
    BitmapFactory.Options opt = new BitmapFactory.Options();
    opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
    opt.inSampleSize = 1;
    Bitmap img = null;
    do {
      try {
        img = BitmapFactory.decodeStream(streamfactory.getStream(), null, opt);
      }
      catch(OutOfMemoryError e) {}
      catch(Exception e) { break; }
      opt.inSampleSize *= 2;
    } while(img == null && opt.inSampleSize <= 16);
    if(img != null && img.getWidth() > 0 && img.getHeight() > 0)
      jniInsertImage(img);
    else
      Toast.makeText(this, R.string.noimage_msg, Toast.LENGTH_SHORT).show();
    doRepaint();
    updateUI();
  }

  private void combinedGetImage()
  {
    Uri outputFileUri = Uri.fromFile(new File(getTempPath(), "_camera.jpg"));
    // capture image (camera) intents - get all and add to list
    final java.util.List<Intent> cameraIntents = new java.util.ArrayList<Intent>();
    final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
    final java.util.List<ResolveInfo> listCam = getPackageManager().queryIntentActivities(captureIntent, 0);
    for(ResolveInfo res : listCam) {
      final Intent intent = new Intent(captureIntent);
      intent.setComponent(new android.content.ComponentName(res.activityInfo.packageName, res.activityInfo.name));
      intent.setPackage(res.activityInfo.packageName);
      intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
      cameraIntents.add(intent);
    }

    // select image intents
    final Intent galleryIntent = new Intent();
    galleryIntent.setType("image/*");
    galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

    // combined intent
    final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.select_image));
    // prepend the camera options
    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new android.os.Parcelable[]{}));
    startActivityForResult(chooserIntent, ID_GETIMG);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent)
  {
    if(requestCode == ID_GETIMG && resultCode == RESULT_OK) {
      if(intent == null || android.provider.MediaStore.ACTION_IMAGE_CAPTURE.equals(intent.getAction()))
        requestCode = ID_CAPTUREIMG;
      else
        requestCode = ID_SELECTIMG;
      // fall through to ID_CAPTUREIMG and ID_SELECTIMG blocks
    }
    if(requestCode == ID_CAPTUREIMG && resultCode == RESULT_OK) {
      // Thanks Java!
      final File streamfile = new File(getTempPath(), "_camera.jpg");
      doInsertImage(new ImageInputStreamFactory() {
        public InputStream getStream() throws FileNotFoundException {
          return new FileInputStream(streamfile);
        }
      });
    }
    else if(requestCode == ID_SELECTIMG && resultCode == RESULT_OK) {
      final Uri streamuri = intent.getData();
      doInsertImage(new ImageInputStreamFactory() {
        public InputStream getStream() throws FileNotFoundException {
          return getContentResolver().openInputStream(streamuri);
        }
      });
    }
    else if((requestCode == ID_EXPORTDOC || requestCode == ID_EXPORTPDF) && resultCode == RESULT_OK) {
      String filePath = intent.getStringExtra(FileBrowser.RESULT_PATH);
      // save the path
      prefEditor.putString("ExportFilePath", filePath);
      prefEditor.apply();
      if(requestCode == ID_EXPORTDOC)
        jniSaveHTML(filePath);
      else if(requestCode == ID_EXPORTPDF)
        jniSavePDF(filePath);
    }
    else if(requestCode == ID_PREFERENCES) {
      // if all prefs were cleared, let's just exit rather than figure out what prefs to restore
      if(!mPrefs.contains("PenType"))
        finish();
      prefsUpdated();
      mModeLocked = false;  // ensure action bar is updated properly
      invalidateOptionsMenu();  // in case some UI options were changed
    }
    super.onActivityResult(requestCode, resultCode, intent);
  }

  // Undo/Redo on volume keys depressed, and prevent any volume key
  //  events (depress or release) from being passed up (which causes phone to beep)
  @Override
  public boolean dispatchKeyEvent(KeyEvent event)
  {
    switch(event.getKeyCode()) {
      case KeyEvent.KEYCODE_BACK:
        if(mBookmarks) {
          if(event.getAction() == KeyEvent.ACTION_DOWN)
            doJNIAction(ID_TOGGLEBOOKMARKS);
          return true;
        }
        else if(mPrefs.getBoolean("DisableBackKey", false))
          return true;
        break;
      case KeyEvent.KEYCODE_VOLUME_UP:
        if(volUpAction == 0)
          break;
        else if(volUpAction == ID_EMULATEPENBTN)
          volUpState = event.getAction();
        else if(event.getAction() == KeyEvent.ACTION_DOWN)
          doJNIAction(volUpAction);
        return true;
      case KeyEvent.KEYCODE_VOLUME_DOWN:
        if(volDownAction == 0)
          break;
        else if(volDownAction == ID_EMULATEPENBTN)
          volDownState = event.getAction();
        else if(event.getAction() == KeyEvent.ACTION_DOWN)
          doJNIAction(volDownAction);
        return true;
    }
    return super.dispatchKeyEvent(event);
  }

}
