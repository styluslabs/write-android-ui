package com.styluslabs.write;

import java.util.Locale;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

public class PenPreviewView extends View
{
  public float penWidth = 0;
  public int penColor = Color.WHITE;
  public float penPressure = 0;
  public boolean penHighlight = false;

  public PenPreviewView(Context context) {
    this(context, null, 0);
  }

  public PenPreviewView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public PenPreviewView(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    //setClickable(true);
  }

  public void setPenProps(int color, float width, float pressure, boolean highlight)
  {
    penColor = color;
    penWidth = width;
    penPressure = pressure;
    penHighlight = highlight;
  }

  // There was a locale dependent bug here because String.format was using ',' as the decimal separator for
  //  some locales, which caused the propstring to be split incorrectly.  Furthermore, parseFloat is not
  //  locale-dependent and doesn't support ',' as a separator.  Soln is to force Locale.US for String.format.
  public void setPenProps(String propstring)
  {
    String[] props = propstring.split(",");
    try {
      penColor = Long.decode(props[0]).intValue();  // accept # and 0x prefixes
      penWidth = Float.parseFloat(props[1]);
      penPressure = Float.parseFloat(props[2]);
      penHighlight = Integer.parseInt(props[3]) != 0;
    }
    catch(Exception e) {}
  }

  public String getPenProps()
  {
    return String.format(Locale.US, "%d,%f,%f,%d", penColor, penWidth, penPressure, penHighlight ? 1 : 0);
  }

  @Override
  protected void onDraw(Canvas canvas)
  {
    canvas.drawColor(Color.WHITE);

    Paint paint = new Paint();
    paint.setAntiAlias(true);
    paint.setStyle(Style.STROKE);
    // how to handle very large widths?
    paint.setStrokeWidth(penWidth);
    paint.setColor(penColor);

    Path path = new Path();
    // normalize coordinates
    //canvas.scale(getWidth(), getHeight());
    int w = getWidth();
    int h = getHeight();
    path.moveTo(0.1f*w, 0.5f*h);
    path.cubicTo(0.3f*w, 0.2f*h, 0.7f*w, 0.8f*h, 0.9f*w, 0.5f*h);
    canvas.drawPath(path, paint);
  }
}
