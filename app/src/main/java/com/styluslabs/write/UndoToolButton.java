package com.styluslabs.write;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;

public class UndoToolButton extends ImageView
{

  public class UndoDialView extends View
  {
    Paint mDialPaint;
    Paint mIndPaint;
    public float mIndAngle = 0;

    public UndoDialView(Context context) {
      //super(context);
      this(context, null, 0);
    }

    public UndoDialView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UndoDialView(Context context, AttributeSet attrs, int defStyle)
    {
      super(context, attrs, defStyle);

      // Paint for main dial
      mDialPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
      mDialPaint.setColor(Color.BLACK);
      mDialPaint.setStyle(Paint.Style.STROKE);
      mDialPaint.setStrokeWidth(3);
      // paint for indicator dot
      mIndPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
      mIndPaint.setColor(Color.BLUE);
      mIndPaint.setStrokeWidth(3);
    }

    public void setIndicatorActive(boolean active)
    {
      if(active)
        mIndPaint.setColor(Color.BLUE);
      else
        mIndPaint.setColor(Color.RED);
    }

    public float getIndicatorAngle(float screenX, float screenY)
    {
      int location[] = new int[2];
      getLocationOnScreen(location);
      //Log.v("WTF!!!", "Location: " + location[0] + ", " + location[1] + " Dims: " + getWidth() + ", " + getHeight());
      // get touch position relative to this view if we know our location
      if(location[0] != 0 || location[1] != 0) {
        float x = screenX - location[0];
        float y = screenY - location[1];
        return -(float)Math.atan2(x - getWidth()/2, y - getHeight()/2);
      }
      else
        return M_PI;
    }

    public void setIndicatorAngle(float angle)
    {
      mIndAngle = angle;
      // force redraw
      invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
      int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
      int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
      int size = Math.min(Math.min(parentWidth, parentHeight), 200);
      setMeasuredDimension(size, size);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
      int a = getWidth()/2;
      canvas.drawCircle(a, a, a - 10, mDialPaint);
      canvas.drawCircle((float)(a + (a-10)*Math.sin(-mIndAngle)),
              (float)(a + (a-10)*Math.cos(-mIndAngle)), 4, mIndPaint);
    }
  }  // UndoDialView

  public interface UndoRedoListener
  {
    void undo();
    void redo();
    boolean canUndo();
    boolean canRedo();
    void undoRedoEnd(boolean doUndo);
  }

  private UndoRedoListener mListener;
  private final PopupWindow mPopupMenu;
  private final UndoDialView mUndoDialView;
  private boolean isPopupOpened = false;

  private final float M_PI = (float)Math.PI;
  private float prevAngle = 0;
  private float stepAngle = 2*M_PI/36;
  private boolean undoOnRelease = false;
  private float downX = -1;
  private float downY = -1;

  private static final int PRESSED_COLOR = 0xBF0000FF;

  public UndoToolButton(Context context) {
      //super(context);
      this(context, null, 0);
  }

  public UndoToolButton(Context context, AttributeSet attrs) {
      this(context, attrs, 0);
  }

  public UndoToolButton(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    setClickable(true);

    mUndoDialView = new UndoDialView(context, attrs);
    // TODO: get rid of PopupWindow fly-in animation
    mPopupMenu = new PopupWindow(mUndoDialView,
        WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
    mPopupMenu.setAnimationStyle(0);  // no animation
  }

  public void setListener(UndoRedoListener cb)
  {
    mListener = cb;
  }

  public void setDialSteps(int steps)
  {
    stepAngle = 2*M_PI/steps;
  }

  // we must wait until popup is displayed
  private void processDownEvent()
  {
    prevAngle = mUndoDialView.getIndicatorAngle(downX, downY);
    mUndoDialView.setIndicatorAngle(prevAngle);
    downX = -1;
    downY = -1;
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    final int action = event.getAction();

    if(isPopupOpened && downX >= 0)
      processDownEvent();

    switch (action) {
    case MotionEvent.ACTION_DOWN:
      if(!isPopupOpened) {
        // show as a dropdown from this view
        //mPopupMenu.showAsDropDown(this);
        // Fucking android ... getWidth = 0 the first time we hit this code since UndoDialView hasn't been
        //  displayed yet, so just assume width = 200; plus showAtLocation won't center horizontally!
        mPopupMenu.showAtLocation(this, Gravity.NO_GRAVITY,
            (int)event.getRawX() - 100, (int)event.getRawY() + 30);
        isPopupOpened = true;
      }
      // UndoDialView hasn't been drawn at this point, so getIndicatorAngle can't get location
      //  and thus can't calculate angle; we have to wait until the next event.  Fucking Android.
      downX = event.getRawX();
      downY = event.getRawY();
      mUndoDialView.setIndicatorAngle(M_PI);
      mUndoDialView.setIndicatorActive(true);
      undoOnRelease = true;
      setBackgroundColor(PRESSED_COLOR);
      return true;
    case MotionEvent.ACTION_MOVE:
      if(!isPopupOpened)
        return true;

      float angle = mUndoDialView.getIndicatorAngle(event.getRawX(), event.getRawY());
      float deltaAngle = angle - prevAngle;
      if(deltaAngle > M_PI)
        deltaAngle -= 2*M_PI;
      else if(deltaAngle < -M_PI)
        deltaAngle += 2*M_PI;

      int delta = (int)(deltaAngle/stepAngle);
      if(delta != 0) {
        mUndoDialView.setIndicatorAngle(angle);
        undoOnRelease = false;
        prevAngle = (prevAngle + delta*stepAngle) % 2*M_PI;
        while(delta > 0 && mListener.canRedo()) {
          mListener.redo();
          delta--;
        }
        while(delta < 0 && mListener.canUndo()) {
          mListener.undo();
          delta++;
        }
        prevAngle = angle;
        mUndoDialView.setIndicatorActive(delta == 0);
      }

      return true;
    case MotionEvent.ACTION_UP:
      if(isPopupOpened)  {
        mPopupMenu.dismiss();
        isPopupOpened = false;
      }
      mListener.undoRedoEnd(undoOnRelease);
      undoOnRelease = false;
      setBackgroundColor(0x00000000);
      return true;
    }
    return false;
  }
}
