package com.styluslabs.write;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Simple notes database access helper class. Defines the basic CRUD operations
 * for the notepad example, and gives the ability to list all notes as well as
 * retrieve or modify a specific note.
 *
 * This has been improved from the first version of this tutorial through the
 * addition of better error handling and also using returning a Cursor instead
 * of using a collection of inner classes (which is less scalable and not
 * recommended).
 */
public class NotesDbAdapter {

    public static final String KEY_ROWID = "_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_BODY = "body";
    public static final String KEY_SORTORDER = "sortorder";
    public static final String KEY_TAGS = "tags";
    public static final String KEY_THUMBNAIL = "thumbnail";
    // timestamps
    public static final String KEY_CREATETIME = "createtime";
    public static final String KEY_MODIFYTIME = "modifytime";

    private static final String TAG = "NotesDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    // Database creation sql statement
    private static final String DATABASE_CREATE =
            "create table drawings (_id integer primary key autoincrement, "
                    + "title text, body text, sortorder integer, "
                    	+	"createtime integer, modifytime integer, tags text, thumbnail blob);";

    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "drawings";
    private static final int DATABASE_VERSION = 3;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
          if(oldVersion <= 2 && newVersion > 2) {
            // SQLite does not support drop column
            //db.execSQL("ALTER TABLE " + DATABASE_TABLE + " DROP COLUMN img;");
            db.execSQL("ALTER TABLE " + DATABASE_TABLE + " ADD COLUMN tags DEFAULT '';");
          }
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param ctx the Context within which to work
     */
    public NotesDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public NotesDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    // reset the database
    public void reset() {
        mDb.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        mDb.execSQL(DATABASE_CREATE);
    }

    /**
     * Create a new note using the title and body provided. If the note is
     * successfully created return the new rowId for that note, otherwise return
     * a -1 to indicate failure.
     *
     * @param title the title of the note
     * @param body the body of the note
     * @return rowId or -1 if failed
     */
    public long createNote(Long sortorder, String title, String body) {
    	return createNote(sortorder, title, body, null, null, 0);
    }

    public long createNote(Long sortorder, String title, String body, String tags, byte[] thumb, long timestamp)
    {
        ContentValues initialValues = new ContentValues();
        if(sortorder != null)
          initialValues.put(KEY_SORTORDER, sortorder);
        if(title != null)
          initialValues.put(KEY_TITLE, title);
        // body cannot be null
        initialValues.put(KEY_BODY, body);
        if(tags != null)
          initialValues.put(KEY_TAGS, tags);
        if(thumb != null && thumb.length > 0)
          initialValues.put(KEY_THUMBNAIL, thumb);
        // timestamps
        if(timestamp <= 0)
          timestamp = new Date().getTime();
        initialValues.put(KEY_CREATETIME, timestamp);
        initialValues.put(KEY_MODIFYTIME, timestamp);

        return mDb.insert(DATABASE_TABLE, null, initialValues);
    }

    /**
     * Delete the note with the given rowId
     *
     * @param rowId id of note to delete
     * @return true if deleted, false otherwise
     */
    public boolean deleteNote(long rowId)
    {
        return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    // deleting a tag means clearing the tags column for all matching notes
    public void deleteTag(String oldtag)
    {
      renameTag(oldtag, "");
    }

    public void renameTag(String oldtag, String newtag)
    {
      ContentValues args = new ContentValues();
      args.put(KEY_TAGS, newtag);
      mDb.update(DATABASE_TABLE, args, "tags = ?", new String[]{ oldtag });
      //mDb.execSQL("UPDATE " + DATABASE_TABLE + " SET tags = ? WHERE tags = ?;", new Object[]{ newtag, oldtag });
    }

    /**
     * Return a Cursor over the list of all notes in the database
     *
     * @return Cursor over all notes
     */
    public Cursor fetchAllNotes(String orderby)
    {
        return mDb.query(DATABASE_TABLE,
            new String[] {KEY_ROWID, KEY_TITLE, KEY_BODY, KEY_TAGS, KEY_THUMBNAIL},  // columns to return
            null, null, null, null, orderby);  // WHERE, args for WHERE, GROUP BY, HAVING, ORDER BY
    }

    // fetch all notes matching a given tag
    public Cursor fetchNotes(String tags, String orderby)
    {
        return mDb.query(DATABASE_TABLE,
            new String[] {KEY_ROWID, KEY_TITLE, KEY_BODY, KEY_TAGS, KEY_THUMBNAIL},  // columns to return
            KEY_TAGS + " = ?",  // WHERE
            new String[] {tags},  // args for WHERE
            null, null, orderby);  // GROUP BY, HAVING, ORDER BY
    }

    public Cursor fetchTags()
    {
        return mDb.query(true, DATABASE_TABLE, // distinct = true
            new String[] {KEY_ROWID, KEY_TAGS},  // columns to return; _id is required for Android adapters
            null, null, null, null, null, null); // WHERE, args for WHERE, GROUP BY, HAVING, ORDER BY
    }

    /**
     * Return a Cursor positioned at the note that matches the given rowId
     *
     * @param rowId id of note to retrieve
     * @return Cursor positioned to matching note, if found
     * @throws SQLException if note could not be found/retrieved
     */
    public Cursor fetchNote(long rowId) throws SQLException
    {
      Cursor mCursor = mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TITLE, KEY_BODY, KEY_TAGS},
          KEY_ROWID + "=" + rowId, null, null, null, null, null);
      if(mCursor != null) {
        mCursor.moveToFirst();
      }
      return mCursor;
    }

    public Cursor fetchNoteWithBody(String body) throws SQLException
    {
      Cursor mCursor = mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TITLE, KEY_BODY, KEY_TAGS},
          KEY_BODY + "='" + body + "'", null, null, null, null, null);
      if(mCursor != null) {
        mCursor.moveToFirst();
      }
      return mCursor;
    }

    // update a single column of a note
    public boolean updateNote(long rowId, String field, String value)
    {
      ContentValues args = new ContentValues();
      args.put(field, value);
      return updateRow(rowId, args);
    }

    public boolean updateNote(long rowId, String title, String body, String tags, byte[] thumb)
    {
        ContentValues args = new ContentValues();
        if(title != null)
          args.put(KEY_TITLE, title);
        if(body != null)
          args.put(KEY_BODY, body);
        if(tags != null)
          args.put(KEY_TAGS, tags);
        if(thumb != null && thumb.length > 0)
          args.put(KEY_THUMBNAIL, thumb);

        // last modify time
        args.put(KEY_MODIFYTIME, new Date().getTime());

        return updateRow(rowId, args);
    }

    public boolean updateRow(long rowId, ContentValues args)
    {
      if(args.size() > 0)
        return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
      else
        return false;
  }
}
