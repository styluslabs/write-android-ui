package com.styluslabs.write;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.styluslabs.colorpicker.ColorPickerDialog;

public class PageSetupDialog extends AlertDialog
  implements OnClickListener, ColorPickerDialog.OnColorChangedListener, OnItemSelectedListener {

  public interface OnPageSetupListener {
      void pageSetup(String str);
  }

  private final Context mContext;
  private final OnPageSetupListener mListener;
  private final String mPrompt;
  private final SharedPreferences mPrefs;

  private final int[] mVals;
  private Spinner mSizeSpinner;
  private Spinner mRuleSpinner;
  private EditText mWidthEdit;
  private EditText mHeightEdit;
  private EditText mXRulingEdit;
  private EditText mYRulingEdit;
  private EditText mMarginEdit;
  private CheckBox cbApplyAll;
  private CheckBox cbDocDefault;
  private CheckBox cbGlobalDefault;

  private int pageColor;
  private int ruleColor;
  private boolean isPageColor;

  public PageSetupDialog(Context context, OnPageSetupListener listener, SharedPreferences prefs, String prompt, int[] vals)
  {
    super(context);
    mContext = context;
    mPrompt = prompt;
    mListener = listener;
    mVals = vals;
    mPrefs = prefs;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
      setTitle(mPrompt);
      View view = getLayoutInflater().inflate(R.layout.page_setup, null);
      // OK, Cancel buttons
      setButton(DialogInterface.BUTTON_POSITIVE,
          mContext.getText(R.string.confirm), this);
      setButton(DialogInterface.BUTTON_NEGATIVE,
          mContext.getText(R.string.cancel), (OnClickListener) null);
      // init fields
      mWidthEdit = (EditText) view.findViewById(R.id.page_width_edit);
      mWidthEdit.setText(Integer.toString(mVals[0]));
      mHeightEdit = (EditText) view.findViewById(R.id.page_height_edit);
      mHeightEdit.setText(Integer.toString(mVals[1]));
      mXRulingEdit = (EditText) view.findViewById(R.id.page_xruling_edit);
      mXRulingEdit.setText(Integer.toString(mVals[2]));
      mYRulingEdit = (EditText) view.findViewById(R.id.page_yruling_edit);
      mYRulingEdit.setText(Integer.toString(mVals[3]));
      mMarginEdit = (EditText) view.findViewById(R.id.page_margin_edit);
      mMarginEdit.setText(Integer.toString(mVals[4]));

      mSizeSpinner = (Spinner) view.findViewById(R.id.page_size_spinner);
      mSizeSpinner.setOnItemSelectedListener(this);
      mRuleSpinner = (Spinner) view.findViewById(R.id.ruling_spinner);
      mRuleSpinner.setOnItemSelectedListener(this);

      cbApplyAll = (CheckBox) view.findViewById(R.id.pageApplyAll);
      cbDocDefault = (CheckBox) view.findViewById(R.id.pageDocDefault);
      cbGlobalDefault = (CheckBox) view.findViewById(R.id.pageGlobalDefault);

      pageColor = mVals[5];
      ruleColor = mVals[6];

      // load current dims to predef arrays
      predefSizes[0][0] = mVals[0];
      predefSizes[0][1] = mVals[1];
      predefRulings[0][0] = mVals[2];
      predefRulings[0][1] = mVals[3];
      predefRulings[0][2] = mVals[4];
      predefRulings[0][3] = ruleColor;

      // load screen dims as predef size ... metrics.density is
      DisplayMetrics metrics = new DisplayMetrics();
      getWindow().getWindowManager().getDefaultDisplay().getMetrics(metrics);
      predefSizes[1][0] = mPrefs.getInt("screenPageWidth", 768);
      predefSizes[1][1] = mPrefs.getInt("screenPageHeight", 1024);
      // landscape
      predefSizes[2][0] = predefSizes[1][1];
      predefSizes[2][1] = predefSizes[1][0];

      Button pagecolorbtn = (Button) view.findViewById(R.id.btnPageColor);
      pagecolorbtn.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
          isPageColor = true;
          ColorPickerDialog pagecolordialog = new ColorPickerDialog(mContext, pageColor);
          pagecolordialog.setOnColorChangedListener(PageSetupDialog.this);
          pagecolordialog.show();
        }
      });

      Button rulecolorbtn = (Button) view.findViewById(R.id.btnRuleColor);
      rulecolorbtn.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
          isPageColor = false;
          ColorPickerDialog rulecolordialog = new ColorPickerDialog(mContext, ruleColor);
          rulecolordialog.setOnColorChangedListener(PageSetupDialog.this);
          rulecolordialog.show();
        }
      });

      setView(view);
      // this cannot be called until after view is set
      super.onCreate(savedInstanceState);
  }

  public void onColorChanged(int color)
  {
    if(isPageColor)
      pageColor = color;
    else
      ruleColor = color;
  }

  // called for OK button click only
  public void onClick(DialogInterface dialog, int which)
  {
    if(mListener != null) {
      int applyAll = cbApplyAll.isChecked() ? 1 : 0;
      int docDefault = cbDocDefault.isChecked() ? 1 : 0;
      int globalDefault = cbGlobalDefault.isChecked() ? 1 : 0;

      if(cbGlobalDefault.isChecked()) {
        SharedPreferences.Editor spedit = mPrefs.edit();
        spedit.putInt("pageWidth", PaintMain.atoi(mWidthEdit.getText().toString(), 768));
        spedit.putInt("pageHeight", PaintMain.atoi(mHeightEdit.getText().toString(), 1024));
        spedit.putInt("xRuling", PaintMain.atoi(mXRulingEdit.getText().toString(), 0));
        spedit.putInt("yRuling", PaintMain.atoi(mYRulingEdit.getText().toString(), 40));
        spedit.putInt("marginLeft", PaintMain.atoi(mMarginEdit.getText().toString(), 100));
        spedit.putInt("pageColor", pageColor);
        spedit.putInt("ruleColor", ruleColor);
        spedit.apply();
      }

      // convert to XML string, pass across JNI, parse with pugi
      String s = String.format("<pagesetup width='%s' height='%s' xruling='%s' yruling='%s' marginLeft='%s' "
          + "color='%d' ruleColor='%d' applyToAll='%d' docDefault='%d' globalDefault='%d'/>",
          mWidthEdit.getText().toString(), mHeightEdit.getText().toString(),
          mXRulingEdit.getText().toString(), mYRulingEdit.getText().toString(),
          mMarginEdit.getText().toString(), pageColor, ruleColor, applyAll, docDefault, globalDefault);

      mListener.pageSetup(s);
      dialog.dismiss();
    }
  }

  // predefined sizes and rulings
  private final int predefSizes[][] = {
    {0, 0}, // to be replaced by current
    {0, 0}, // device screen (portrait) - set in constructor
    {0, 0}, // device screen (landscape)
    {1190, 1540}, // letter
    {700, 1120}, // 5 x 8
    {420, 700}  // 3 x 5
  };

    // {x ruling, y ruling, left margin, rule color}
  private final int predefRulings[][] = {
    {0, 0, 0, 0},  // to be replaced by current
    {0, 0, 0, 0xFF0000FF},  // plain
    {0, 45, 100, 0xFF0000FF},  // wide ruled
    {0, 40, 100, 0xFF0000FF},  // medium ruled
    {0, 35, 100, 0xFF0000FF},  // narrow ruled
    {35, 35, 0, 0xFF7F7FFF},  // coase grid
    {30, 30, 0, 0xFF7F7FFF},  // medium grid
    {20, 20, 0, 0xFF7F7FFF}  // fine grid
  };

  // OnItemSelectedListener interface
  public void onItemSelected(AdapterView<?> arg0, View view, int pos, long id)
  {
    if(arg0 == mSizeSpinner && pos < predefSizes.length) {
      mWidthEdit.setText(String.valueOf(predefSizes[pos][0]));
      mHeightEdit.setText(String.valueOf(predefSizes[pos][1]));
    }
    else if(arg0 == mRuleSpinner && pos < predefRulings.length) {
      mXRulingEdit.setText(String.valueOf(predefRulings[pos][0]));
      mYRulingEdit.setText(String.valueOf(predefRulings[pos][1]));
      mMarginEdit.setText(String.valueOf(predefRulings[pos][2]));
      ruleColor = predefRulings[pos][3];
    }
  }

  public void onNothingSelected(AdapterView<?> arg0) {}

}
