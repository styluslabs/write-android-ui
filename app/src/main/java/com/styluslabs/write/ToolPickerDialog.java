package com.styluslabs.write;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.styluslabs.colorpicker.ColorPickerDialog;

// original file is from Android sample project called ApiDemos
public class ToolPickerDialog extends AlertDialog implements ColorPickerDialog.OnColorChangedListener, OnClickListener
{

  private static final int COLOR_HIYELLOW = 0x7fffff00;
  private static final int COLOR_HIGREEN = 0x7f00ff00;
  private static final int COLOR_HIBLUE = 0x7f0000ff;
  private static final int COLOR_HIRED = 0x7fff0000;
  //private static final int COLOR_DARKGREEN = 0xff008000;
  //Color.TRANSPARENT, Color.TRANSPARENT + 1,
  protected int[] mColors = {
      0xFF000000, 0xFF3F3F3F, 0xFF7F7F7F, 0xFFBFBFBF, 0xFFFFFFFF,
      //0xFF0F0000, 0xFF3F0000,
      0xFF7F0000, 0xFFBF0000, 0xFFFF0000, 0xFFFF7F7F, 0xFFFFBFBF, // red
      //0xFF000F00, 0xFF003F00,
      0xFF007F00, 0xFF00BF00, 0xFF00FF00, 0xFF7FFF7F, 0xFFBFFFBF, // green
      //0xFF00000F, 0xFF00003F,
      0xFF00007F, 0xFF0000BF, 0xFF0000FF, 0xFF7F7FFF, 0xFFBFBFFF, // blue
      0xFFFF4500, 0xFFFF8C00, 0xFFFFA500, 0xFFFFD200, 0xFFFFFF00,  // yellow/orange
      COLOR_HIYELLOW, COLOR_HIGREEN, COLOR_HIBLUE, COLOR_HIRED, Color.TRANSPARENT};  // highlighters


  public interface OnToolChangedListener
  {
    void toolChanged(int color, float width, float pressure, boolean highlight, int saveslot);
  }

  private SeekBar mSeekBar;
  private EditText mPenSizeText;
  private Spinner mSaveSpin;
  private Spinner mPressureSpin;
  private ViewGroup mPenOnlyGroup;
  private final Context mContext;
  private final OnToolChangedListener mListener;
  //private ButtonAdapter mButtonAdapter;
  private ImageView mSelectedView;
  private float mSelectedWidth;
  private int mSelectedColor;
  private boolean mHighlight;
  private final float mPressure;
  // with localization, it is too difficult to reliably parse text from pressure spinner
  private final float mPressureVals[] = {0f, 0.25f, 0.5f, 1f};

  public ToolPickerDialog(Context context, OnToolChangedListener listener,
      int color, float width, float pressure, boolean highlight)
  {
    super(context);
    mContext = context;
    mListener = listener;
    mSelectedColor = color;
    mSelectedWidth = width;
    mHighlight = highlight;
    mPressure = pressure;
    mSelectedView = null;
  }

  // must be between 0 and 1
  private static final float beta = 0.9f;

  // these fns assume SeekBar range is 0 to 200 and width range is 0 to 200
  private float seekBarToWidth(float x)
  {
    x = x/200;
    float y = (beta - 1)*x/(beta*x - 1);
    float w = y*200f; // + 0.5f;
    if(w < 5)
      return Math.round(w*10f)/10f;
    else if(w < 10)
      return Math.round(w*2f)/2f;
    else
      return Math.round(w);
    // (int)(0.5f + x*x/50); //(Math.exp(x - 20 + ln199) + 1);
  }

  private int widthToSeekBar(float w)
  {
    w = w/200;
    float x = w/(beta*w - beta + 1);
    return Math.round(x*200);
  }

  // for custom color selector
  public void onColorChanged(int color)
  {
    mSelectedColor = color;
  }

  private void allDone()
  {
    int saveslot = PaintMain.atoi(mSaveSpin.getSelectedItem().toString(), 0) - 1;
    //float pressure = PaintMain.atof(mPressureSpin.getSelectedItem().toString().split(" ")[0], 0);
    int prpos = mPressureSpin.getSelectedItemPosition();
    float pressure = (prpos >= 0 && prpos < mPressureVals.length) ? mPressureVals[prpos] : 0f;
    mListener.toolChanged(mSelectedColor, mSelectedWidth, pressure, mHighlight, saveslot);
    dismiss();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    // super.onCreate(savedInstanceState);
    // setContentView(R.layout.tool_picker);
    setTitle(R.string.pen_selection_title);
    View view = getLayoutInflater().inflate(R.layout.tool_picker, null);
    // OK, Cancel buttons
    setButton(DialogInterface.BUTTON_POSITIVE,
        mContext.getText(R.string.confirm), this);
    setButton(DialogInterface.BUTTON_NEGATIVE,
        mContext.getText(R.string.cancel), (OnClickListener)null);

    // items to hide for bookmark
    mPenOnlyGroup = (ViewGroup) view.findViewById(R.id.pen_only_group);

    // setup save pen spinner
    ArrayAdapter<String> adapter =
        new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    adapter.add("None");
    for(int ii = 1; ii <= 6; ii++)
      adapter.add("" + ii);
    mSaveSpin = (Spinner) view.findViewById(R.id.save_pen_spinner);
    mSaveSpin.setAdapter(adapter);

    // setup pressure sensitivity spinner
    // setup save pen spinner
    ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
        mContext.getResources().getStringArray(R.array.tp_pressurevals));
    adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    mPressureSpin = (Spinner) view.findViewById(R.id.pressure_spinner);
    mPressureSpin.setAdapter(adapter2);
    mPressureSpin.setSelection(mPressure > 0.75 ? 3 : (mPressure > 0.375 ? 2 : (mPressure > 0.125 ? 1 : 0)));

    LinearLayout mColorGrid = (LinearLayout) view.findViewById(R.id.color_grid);
    LinearLayout row = null;
    for(int ii = 0; ii < mColors.length; ii++) {
      if(ii % 5 == 0) {
        row = new LinearLayout(mContext);
        row.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams lrow = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        lrow.weight = 1;
        mColorGrid.addView(row, lrow);
      }
      ImageView imageView = new ImageView(mContext);
      LinearLayout.LayoutParams limage = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
      limage.weight = 1;
      limage.setMargins(2, 2, 2, 2);
      imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
      // this determines how much larger background is than image
      imageView.setPadding(2, 2, 2, 2);
      imageView.setTag(ii);
      imageView.setOnClickListener(new android.view.View.OnClickListener() {
        public void onClick(View v)
        {
          int id = (Integer) v.getTag();
          int color = mColors[id];
          if(mSelectedView != null)
            mSelectedView.setBackgroundDrawable(new ColorDrawable(mSelectedColor));
          if(color == Color.TRANSPARENT) {
            mSelectedView = null;
            ColorPickerDialog pencolordialog = new ColorPickerDialog(mContext, mSelectedColor);
            pencolordialog.setOnColorChangedListener(ToolPickerDialog.this);
            pencolordialog.setAlphaSliderVisible(true);
            pencolordialog.show();
          }
          else {
            mSelectedView = (ImageView)v;
            Drawable[] layers = { new ColorDrawable(Color.WHITE), new ColorDrawable(Color.BLACK), new ColorDrawable(color) };
            LayerDrawable ld = new LayerDrawable(layers);
            ld.setLayerInset(1, 2, 2, 2, 2);
            ld.setLayerInset(2, 3, 3, 3, 3);
            mSelectedView.setBackgroundDrawable(ld);
            mSelectedColor = color;
            boolean oldhl = mHighlight;
            mHighlight = Color.alpha(color) < 255;
            if(mHighlight && !oldhl)
              mPenSizeText.setText("40.0");
          }
        }
      });

      int c = mColors[ii];
      if(c == mSelectedColor) {
        mSelectedView = imageView;
        Drawable[] layers = { new ColorDrawable(Color.WHITE), new ColorDrawable(Color.BLACK), new ColorDrawable(c) };
        LayerDrawable ld = new LayerDrawable(layers);
        ld.setLayerInset(1, 2, 2, 2, 2);
        ld.setLayerInset(2, 3, 3, 3, 3);
        mSelectedView.setBackgroundDrawable(ld);
      }
      else
        imageView.setBackgroundDrawable(new ColorDrawable(c));
      if(c == Color.TRANSPARENT)
        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_ellipsis));
      else if(Color.alpha(c) < 255)
        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_hilite));
      row.addView(imageView, limage);
    }

    // setup stroke width selector
    mSeekBar = (SeekBar) view.findViewById(R.id.pensize_seekbar);
    mPenSizeText = (EditText) view.findViewById(R.id.pensize_edit);
    if(mSelectedWidth < 0) {
      // Bookmark ... hide width selector and save slot
      mPenOnlyGroup.setVisibility(View.GONE);
    }
    else {
      mSeekBar.setProgress(widthToSeekBar(mSelectedWidth));
      mPenSizeText.setText(String.format("%.1f", mSelectedWidth));
      // callback for stroke width selection SeekBar
      mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
          if(fromUser) {
            mSelectedWidth = seekBarToWidth(progress);
            mPenSizeText.setText(String.format("%.1f", mSelectedWidth));
          }
        }
        public void onStartTrackingTouch(SeekBar seekBar) {}
        public void onStopTrackingTouch(SeekBar seekBar) {}
      });

      mPenSizeText.addTextChangedListener(new TextWatcher() {
        public void onTextChanged(CharSequence s, int start, int before, int count) {}
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void afterTextChanged(Editable e) {
          float w = PaintMain.atof(mPenSizeText.getText().toString(), -1);
          if(w >= 0 && w <= 200) {
            mSelectedWidth = w;
            mSeekBar.setProgress(widthToSeekBar(mSelectedWidth));
          }
        }
      });
    }
    setView(view);
    // this cannot be called until after view is set
    super.onCreate(savedInstanceState);
  }

  public void onClick(DialogInterface dialog, int which)
  {
    allDone();
  }
}
