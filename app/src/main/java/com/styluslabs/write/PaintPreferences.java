package com.styluslabs.write;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.widget.Toast;

// Thankfully, with Honeycomb, Google has addressed my main complaints with preferences: not enough
//  boilerplate code and not enough XML!  You can never have too much XML!

public class PaintPreferences extends PreferenceActivity
{
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  protected boolean isValidFragment(String fragmentName) {
    return true;
  }

  @Override
  public void onBuildHeaders(List<Header> target) {
      loadHeadersFromResource(R.xml.preferences, target);
  }


  public static class DocPrefs extends PreferenceFragment
  {
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.doc_prefs);
    }
  }

  public static class InputPrefs extends PreferenceFragment
  {
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.input_prefs);
    }
  }

  public static class InterfacePrefs extends PreferenceFragment
  {
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.interface_prefs);
    }
  }

  public static class DrawingPrefs extends PreferenceFragment
  {
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.drawing_prefs);
    }
  }

  public static class AdvancedPrefs extends PreferenceFragment
  {
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.advanced_prefs);
      mContext = getActivity();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference)
    {
      if(preference.getKey().equals("DeleteDB")) {
        new AlertDialog.Builder(mContext).setIcon(R.drawable.ic_dialog_alert)
        .setTitle(R.string.db_clear_title)
        .setMessage(R.string.db_clear_prompt)
        .setPositiveButton(R.string.yes,
          new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              NotesDbAdapter mDbHelper = new NotesDbAdapter(mContext);
              mDbHelper.open();
              mDbHelper.reset();
              mDbHelper.close();
              Toast.makeText(mContext, R.string.db_cleared_msg, Toast.LENGTH_SHORT).show();
            }
          })
        .setNegativeButton(R.string.no,
          new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {}
          }).show();
        return true;
      }
      else if(preference.getKey().equals("ResetPrefs")) {
        //PreferenceManager.getDefaultSharedPreferences(mContext).edit().clear().commit();
        new AlertDialog.Builder(mContext).setIcon(R.drawable.ic_dialog_alert)
        .setTitle(R.string.resetprefs_title)
        .setMessage(R.string.resetprefs_prompt)
        .setPositiveButton(R.string.yes,
          new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              PreferenceManager.getDefaultSharedPreferences(mContext).edit().clear().commit();
              Toast.makeText(mContext, R.string.resetprefs_msg, Toast.LENGTH_SHORT).show();
            }
          })
        .setNegativeButton(R.string.no,
          new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {}
          }).show();
        return true;
      }
      else if(preference.getKey().equals("ImportExisting")) {
        Intent intent = new Intent(mContext, ImportActivity.class);
        intent.putExtra(ImportActivity.IMPORT_PATH,
            PreferenceManager.getDefaultSharedPreferences(mContext).getString("SaveImgPath", ""));
        startActivity(intent);
        return true;
      }
      else if(preference.getKey().equals("ShowChangelog")) {
        PaintMain.showChangelog(mContext);
        return true;
      }
      else
        return super.onPreferenceTreeClick(preferenceScreen, preference);
      /* else if(preference == mRateApp) {
      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse(getString(R.string.marketlink)));
      startActivity(intent);
      return true; } */
    }
  }
}
