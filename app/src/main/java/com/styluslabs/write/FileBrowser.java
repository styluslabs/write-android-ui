package com.styluslabs.write;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

// Refs:
//  http://code.google.com/p/android-file-dialog/
//  http://www.anddev.org/advanced-tutorials-f21/android-filebrowser-v-2-0-t101.html
//  http://code.google.com/p/openintents

public class FileBrowser extends ListActivity implements EditTextDialog.OnTitleSetListener
{
	public static final String START_PATH = "START_PATH";
	public static final String START_NAME = "START_NAME";
	public static final String RESULT_PATH = "RESULT_PATH";
	public static final String MODE = "MODE";

	// open mode: select button says Open
	// save mode: cannot select a file; select button says Save; opens dialog to enter name
	public static final int MODE_OPEN = 1;
	public static final int MODE_SAVE = 2;
	private int mMode = MODE_OPEN;

	private final String root = "/";
	private String currentPath = root;
	private String startPath;
	private ArrayList<String> mDirs;
	private ArrayList<String> mFiles;
	private File selectedFile;
	private int selectedPos = -1;
	// widgets
	private TextView myPath;
	private EditText mFileName;
	private Button selectButton;
	private Button cancelSelButton;
	private Button cancelCreateButton;
	private Button createButton;

	private LinearLayout layoutSelect;
	private LinearLayout layoutCreate;
	//private InputMethodManager inputManager;


	// based on EfficientAdapter from ApiDemos List14.java
	private class FileBrowserAdapter extends BaseAdapter
	{
        private final LayoutInflater mInflater;
        private final Bitmap mFolderIcon;
        private final Bitmap mFileIcon;

        private final ColorDrawable hilite;

        class ViewHolder {
            TextView text;
            ImageView icon;
        }

        public FileBrowserAdapter(Context context)
        {
            // Cache the LayoutInflate to avoid asking for a new one each time.
            mInflater = LayoutInflater.from(context);

            // Icons bound to the rows.
            mFolderIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_folder);
            mFileIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_file);
            // TODO: better color?
            hilite = new ColorDrawable(0xFF7F3F00);
        }

        public int getCount() {
            return mDirs.size() + mFiles.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent)
        {
            // A ViewHolder keeps references to children views to avoid unnecessary calls
            // to findViewById() on each row.
            ViewHolder holder;

            // When convertView is not null, we can reuse it directly, there is no need
            // to reinflate it. We only inflate a new View when the convertView supplied
            // by ListView is null.
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.file_browser_row, null);
                // Creates a ViewHolder and store references to the two children views
                // we want to bind data to.
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.fdrowtext);
                holder.icon = (ImageView) convertView.findViewById(R.id.fdrowimage);
                convertView.setTag(holder);
            } else {
                // Get the ViewHolder back to get fast access to the TextView
                // and the ImageView.
                holder = (ViewHolder) convertView.getTag();
            }

            // Bind the data efficiently with the holder.
    		if(position < mDirs.size()) {
    			holder.text.setText(mDirs.get(position));
    		    holder.icon.setImageBitmap(mFolderIcon);
    		} else {
    			holder.text.setText(mFiles.get(position - mDirs.size()));
    		    holder.icon.setImageBitmap(mFileIcon);
    		}

    		// I'm intentionally defeating Android's "no concept selection in touch mode"
    		//  religion because I don't want accidental touches to open files
    		if(position == selectedPos)
    			convertView.setBackgroundDrawable(hilite);
    		else
    			convertView.setBackgroundDrawable(null);

            return convertView;
        }

    }

	private FileBrowserAdapter mAdapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setResult(RESULT_CANCELED, getIntent());

		mDirs = new ArrayList<String>();
		mFiles = new ArrayList<String>();
		mAdapter = new FileBrowserAdapter(this);

		setContentView(R.layout.file_browser_main);
		myPath = (TextView) findViewById(R.id.path);
		mFileName = (EditText) findViewById(R.id.fdEditTextFile);
		//inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    layoutSelect = (LinearLayout) findViewById(R.id.fdLinearLayoutSelect);
    layoutCreate = (LinearLayout) findViewById(R.id.fdLinearLayoutCreate);

		selectButton = (Button) findViewById(R.id.fdButtonSelect);
		selectButton.setEnabled(false);
		selectButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (selectedFile != null) {
					getIntent().putExtra(RESULT_PATH, selectedFile.getPath());
					setResult(RESULT_OK, getIntent());
					finish();
				}
			}
		});

		cancelSelButton = (Button) findViewById(R.id.fdButtonCancelSel);
		cancelSelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) { finish();	}
		});

    cancelCreateButton = (Button) findViewById(R.id.fdButtonCancelCreate);
    cancelCreateButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) { finish(); }
    });

		createButton = (Button) findViewById(R.id.fdButtonCreate);
		createButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mFileName.getText().length() > 0) {
					getIntent().putExtra(RESULT_PATH,
							currentPath + "/" + mFileName.getText());
					setResult(RESULT_OK, getIntent());
					finish();
				}
			}
		});

		mMode = getIntent().getIntExtra(MODE, MODE_OPEN);
    if(mMode == MODE_OPEN) {
      layoutCreate.setVisibility(View.GONE);
      setTitle(R.string.menu_openfile);
    }
    else if(mMode == MODE_SAVE) {
      layoutSelect.setVisibility(View.GONE);
      setTitle(R.string.menu_savefile);
      String startName = getIntent().getStringExtra(START_NAME);
      if(startName != null)
        ((TextView)findViewById(R.id.fdEditTextFile)).setText(startName);
    }

		startPath = getIntent().getStringExtra(START_PATH);
		if(startPath == null) startPath = root;
		getDir(startPath);

		mAdapter = new FileBrowserAdapter(this);
	    setListAdapter(mAdapter);
	}

  public void titleSet(String title)
  {
    // nothing for now
  }

	private void getDir(String dirPath)
	{
	  File f = new File(dirPath);
	  // need the while() to handle case of deleted path
	  while(f != null && !f.isDirectory())
	    f = f.getParentFile();
	  if(f == null)
	    f = new File("/");
		getDir(f);
	}

	private void getDir(File f)
	{
		String seldir = null;
		File[] filelist = f.listFiles();
		mDirs.clear();
		mFiles.clear();

		if (!f.getPath().equals(root))
			mDirs.add("../");

		if(filelist != null) {
		  // ... which can happen if sdcard is not accessible
  		for(File file : filelist) {
  			// TODO?: if(file.canRead() && !file.isHidden()) {
  			if(file.isDirectory()) {
  				mDirs.add(file.getName() + "/");
  				if(file.getPath().equals(currentPath))
  					seldir = file.getName() + "/";
  			}
  			else
  				mFiles.add(file.getName());
  		}
		}
		Collections.sort(mDirs, String.CASE_INSENSITIVE_ORDER);
		Collections.sort(mFiles, String.CASE_INSENSITIVE_ORDER);
		currentPath = f.getPath();
		myPath.setText(getText(R.string.location) + ": " + currentPath);

		mAdapter.notifyDataSetChanged();
		// have to wait until lists are sorted! - this just scrolls item into view; no highlighting
		if(seldir != null)
			getListView().setSelection(mDirs.indexOf(seldir));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		File file;
		if(position < mDirs.size())
			file = new File(currentPath, mDirs.get(position));
		else
			file = new File(currentPath, mFiles.get(position - mDirs.size()));

		if(file.isDirectory()) {
			unselect();
			if(file.getName().equals(".."))
				file = new File(currentPath).getParentFile();

			if (file.canRead()) {
				// tried using getCanonialPath() instead of getPath() in getDir(). It was a disaster.
				//if(file.getName().equals(".."))
				//	getDir(new File(currentPath).getParentFile());
				//else
				getDir(file);
			} else {
				alertMessage("[" + file.getName() + "] "
		            + getText(R.string.cant_read_folder));
			}
		} else if(mMode == MODE_OPEN) {
			selectedFile = file;
			//v.setSelected(true); - this does something, but apparently isn't supposed to...
			selectedPos = position;
			selectButton.setEnabled(true);
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if((keyCode == KeyEvent.KEYCODE_BACK)) {
			unselect();
			//if(layoutCreate.getVisibility() == View.VISIBLE) {
			//	layoutCreate.setVisibility(View.GONE);
			//	layoutSelect.setVisibility(View.VISIBLE);
			//} else {
				if(currentPath.equals(startPath) || currentPath.equals(root))
					finish();  // back from root or startPath exits
				else
					getDir(new File(currentPath).getParentFile());
				//return super.onKeyDown(keyCode, event);
			//}
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	private void unselect()
	{
		selectedPos = -1;
		selectButton.setEnabled(false);
	}

	private void alertMessage(String msg)
	{
		new AlertDialog.Builder(this).setIcon(R.drawable.ic_dialog_alert)
		.setTitle(R.string.error)
		.setMessage(msg)
		.setPositiveButton(R.string.confirm,
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}
			}).show();
	}
}
